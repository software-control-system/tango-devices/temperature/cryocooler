
//- Project : Cryocooler, ACCEL/CRYOTHERM cryogenic cooler for optics at SOLEIL
//- file : CryoCoolerInterface.cpp
//- threaded reading of the PLC Data Block
//- Writes on the PLC

//- Local CryoCoolerInterface (not the 1 in PLCServerProxyHelper!)
#include "CryoCoolerInterface.h"
#include <DBOffsets.h>

namespace CryoCooler_ns
{
  //-----------------------------------------------
  //- Ctor ----------------------------------------
  //-----------------------------------------------
  CryoCoolerInterface::CryoCoolerInterface ( Tango::DeviceImpl * _host_device, 
						std::string _plc_name,
						short _dbread,
						short _dbwrite,
						size_t _periodic_exec_ms,
						std::string _cryoCoolerProtocol)
        :yat4tango::DeviceTask(_host_device),
         host_dev(_host_device),
         plc_name(_plc_name),
         db_read_number(_dbread),
         db_write_number(_dbwrite),
         periodic_timeout_ms(_periodic_exec_ms),
         cryocooler_protocol(_cryoCoolerProtocol)
  {
    DEBUG_STREAM << "CryoCoolerInterface::CryoCoolerInterface <- " << std::endl;

    hwp = NULL;
    hwp_wr = NULL;				
	
    //- yat::Task configure optional msg handling
    this->enable_timeout_msg(false);
    this->enable_periodic_msg(true);
    this->set_periodic_msg_period(periodic_timeout_ms);
	
    this->com_error = 0;
    this->com_success = 0;
    this->com_state = HWProxy_ns::HWP_NO_ERROR;
    this->com_status = "HWP_NO_ERROR";
  }

  //-----------------------------------------------
  //- Dtor ----------------------------------------
  //-----------------------------------------------
  CryoCoolerInterface::~CryoCoolerInterface (void)
  {
    DEBUG_STREAM << "CryoCoolerInterface::~CryoCoolerInterface <- " << std::endl;

    if (hwp)
    {
      this->hwp->exit();
      hwp = NULL;
    }

    if (hwp_wr)
    {
      this->hwp_wr->exit();
      hwp_wr = NULL;
    }	
  }

  //------------------------------------------------------
  //- initialize 
  //------------------------------------------------------- 
  void CryoCoolerInterface::initialize()
    throw(Tango::DevFailed)
  {
    DEBUG_STREAM << " CryoCoolerInterface::initialize_DB try to get device proxy on  " << plc_name << std::endl;

    if (hwp)
      return;

    try
    {
      DEBUG_STREAM << "CryoCoolerInterface::initialize trying to create hwp" << std::endl;    
      HWProxy_ns:: HWProxy::Config conf;
      //- this one gets the 100 last WORDS (from 0 to 200)
      conf.url 		         = plc_name;
      conf.device_name         = host_dev->get_name () +"_R";
      conf.db_number           = db_read_number;
      conf.input_offset        = DB_START_INPUT_OFFSET;
      // V2 Management: have to read until offset 200.. => len = 101
      if (cryocooler_protocol.compare(V1_CC_PROTOCOL) == 0)
      { // V1
	      conf.input_length        = (DB_START_INPUT_LEN + DB_END_INPUT_LEN);
      }
      else
      { // V2
        conf.input_length        = DB_END_INPUT_OFFSET_V2;
      }
      conf.output_offset       = DB_END_OUTPUT_OFFSET;
      conf.output_length       = DB_END_OUTPUT_LEN;
      conf.periodic_timeout_ms = periodic_timeout_ms;
		
      hwp = new HWProxy_ns::HWProxy (host_dev, conf);
      if (!hwp)
      {
        ERROR_STREAM << "CryoCoolerInterface::initialize could not instanciate hwp_rd_end ..." << std::endl;
        throw std::bad_alloc();
      }
      hwp->go ();
    }
    catch (std::bad_alloc)
    {
      FATAL_STREAM << "CryoCoolerInterface::initialize () catched bad_alloc" << std::endl;
      this->com_state = HWProxy_ns::HWP_INITIALIZATION_ERROR;
      this->last_error = "could not instanciate hwp_rd_end class catched bad_alloc";
      return;
    }
    catch (...)
    {
      FATAL_STREAM << "CryoCoolerInterface::initialize () catched ..." << std::endl;
      this->com_state = HWProxy_ns::HWP_INITIALIZATION_ERROR;
      this->last_error = "could not instanciate hwp_rd_end class catched (...)";
      return;
    }

    if (hwp_wr)
      return;
				
    try
    {
      DEBUG_STREAM << "CryoCoolerInterface::initialize trying to create hwp_wr" << std::endl;    
      HWProxy_ns:: HWProxy::Config conf;		
      //- Write DB Data the 100 WORDS (from 0 to 100)
      conf.url 		         = plc_name;
      conf.device_name         = host_dev->get_name () +"_W";
      conf.db_number           = db_write_number;

      // V2 Management: DB write is 6 bytes long...
      if (cryocooler_protocol.compare(V1_CC_PROTOCOL) == 0)
      { // V1
        conf.input_offset        = DB_WRITE_INPUT_OFFSET;
        conf.input_length        = DB_WRITE_INPUT_LEN;
        conf.output_offset       = DB_WRITE_OUTPUT_OFFSET;
        conf.output_length       = DB_WRITE_OUTPUT_LEN;
      }
      else
      { // V2
        conf.input_offset        = DB_WRITE_INPUT_OFFSET;
        conf.input_length        = DB_WRITE_INPUT_LEN_V2;
        conf.output_offset       = DB_WRITE_OUTPUT_OFFSET;
        conf.output_length       = DB_WRITE_OUTPUT_LEN_V2;
      }

      conf.periodic_timeout_ms = periodic_timeout_ms;		
	
      hwp_wr = new HWProxy_ns::HWProxy  (host_dev,conf);
      if (!hwp_wr)
      {
	      ERROR_STREAM << "CryoCoolerInterface::initialize could not instanciate hwp_wr ..." << std::endl;
        throw std::bad_alloc();
      }
      hwp_wr->go ();
    }
    catch (std::bad_alloc)
    {
      FATAL_STREAM << "CryoCoolerInterface::initialize () catched bad_alloc" << std::endl;
      this->com_state = HWProxy_ns::HWP_INITIALIZATION_ERROR;
      this->last_error = "could not instanciate hwp_wr class catched bad_alloc";
      return;
    }
    catch (...)
    {
      FATAL_STREAM << "CryoCoolerInterface::initialize () catched ..." << std::endl;
      this->com_state = HWProxy_ns::HWP_INITIALIZATION_ERROR;
      this->last_error = "could not instanciate hwp_rd_end class catched (...)";
      return;
    }
  }
	
  //------------------------------------------------------
  //------------------------------------------------------
  
  
  //-----------------------------------------------
  //- the user core of the Task -------------------
  //-----------------------------------------------
  void CryoCoolerInterface::process_message (yat::Message& _msg)
	throw (Tango::DevFailed)
  {
    //- The DeviceTask's lock_ -------------

    //DEBUG_STREAM << "CryoCoolerInterface::process_message::receiving msg " << _msg.to_string() << std::endl;

    //- handle msg
    switch (_msg.type())
    {
      //- THREAD_INIT =======================
    case yat::TASK_INIT:
      {
        DEBUG_STREAM << "CryoCoolerInterface::process_message::THREAD_INIT::thread is starting up" << std::endl;
        
      } 
      break;
      //- TASK_EXIT =======================
    case yat::TASK_EXIT:
      {
        DEBUG_STREAM << "CryoCoolerInterface::process_message handling TASK_EXIT thread is quitting" << std::endl;

      }
      break;
      //- TASK_PERIODIC ===================
    case yat::TASK_PERIODIC:
      {
        //DEBUG_STREAM << "CryoCoolerInterface::process_message handling TASK_PERIODIC msg" << std::endl;
        //- code relative to the task's periodic job goes here
        this->periodic_job_i();
      }
      break;


      //- TASK_TIMEOUT ===================
    case yat::TASK_TIMEOUT:
      {
        //- code relative to the task's tmo handling goes here
        //DEBUG_STREAM << "CryoCoolerInterface::process_message handling TASK_TIMEOUT msg" << std::endl;
      }
      break;
	  
      //- USER_DEFINED_MSG ================
      //
      // ---------------  INTERFACE TO V2 CRYOCOOLER AS DESCRIBED BY CTRLRFC-633  -----------------------------------------------
      //--------------------------------------------------------------------------------------------------------------------------
	  
       //- SET_COMMAND  ----------------
    case CryoCooler_ns::SET_COMMAND_MSG:
      {
        DEBUG_STREAM << "CryoCoolerInterface::process_message handling SET_COMMAND_MSG user msg" << std::endl;
        //- get msg data...
        struct HWProxy_ns::WInt * wi = 0;
        _msg.detach_data(wi);
        if (wi) 
        {
          try
          {
            DEBUG_STREAM << "CryoCoolerInterface::process_message (1) trying to set command request  " << wi->byte_offset << std::endl;
            if (hwp_wr)
            {
              // V2_COMMAND_REQUEST_BYTE = 0 in the DBW
              // wi->byte_offset contains the command word (2 bytes) as defined in the RI command table, e.g. OPEN_V9 BYTE B#16#6 
			  // the second byte of the command word should contain the handshake flag set to 1, i.e.0x01
              // we have to write the 6 bytes (3 words) of the DBW all at once, e.g. 06 01 00 00 00 00
              short cmd = wi->byte_offset;
              cmd = cmd << 8;
              cmd += 1;
	  
			  short dbw[V2_COMMAND_REQUEST_DBW_SIZE];
			  dbw[0] = cmd;
			  dbw[1] = 0;
			  dbw[2] = 0;
			  
              hwp_wr->write_shorts ("CryoCoolerInterface::process_message (1) trying to write 6 BYTES", V2_COMMAND_REQUEST_BYTE, 
			    V2_COMMAND_REQUEST_DBW_SIZE, &dbw[0]);
            }
          }
          catch (...)
          {
            yat::AutoMutex<yat::Mutex> guard (this->m_com_lock);
            this->com_status = hwp_wr->get_com_status();
            this->com_state = hwp_wr->get_com_state();
            this->last_error = hwp_wr->get_last_error();
            delete wi;
            break;
          }
          delete wi;
        }
        break;
      }
	  
    case CryoCooler_ns::SET_REAL_V2_MSG:
      {
        DEBUG_STREAM << "CryoCoolerInterface::process_message handling SET_REAL_V2_MSG user msg" << std::endl;
       //- get msg data...
        struct HWProxy_ns::WReal * wr = 0;
        _msg.detach_data(wr);
        if (wr) 
        {	
          try
          {
            DEBUG_STREAM << "CryoCoolerInterface::process_message (1) trying to write REAL value " << wr->value << " for command request " << wr->byte_offset << std::endl;
            if (hwp_wr)
            {
              // V2_COMMAND_REQUEST_BYTE = 0 in the DBW
              // wr->byte_offset contains the command word as defined in the RI command table, e.g. SET_V10 BYTE B#16#10
			  // the second byte of the command word should contain the handshake flag set to 1, i.e.0x01
              // we have to write the 6 bytes (3 words) of the DBW all at once, e.g. 10 01 42 50 00 00
              short cmd = wr->byte_offset;
              cmd = cmd << 8;
              cmd += 1;

              long * l = (long *) &wr->value;
			  short dbw[V2_COMMAND_REQUEST_DBW_SIZE];
			  dbw[0] = cmd;
			  dbw[1] = (short)((*l & 0xFFFF0000) / 0x10000);
			  dbw[2] = (short)(*l & 0x0000FFFF);
			  
              hwp_wr->write_shorts ("CryoCoolerInterface::process_message (1) trying to write 6 BYTES", V2_COMMAND_REQUEST_BYTE, 
			    V2_COMMAND_REQUEST_DBW_SIZE, &dbw[0]);			  
            }
          }
          catch (...)
          {
            yat::AutoMutex<yat::Mutex> guard (this->m_com_lock);
            this->com_status = hwp_wr->get_com_status();
            this->com_state = hwp_wr->get_com_state();
            this->last_error = hwp_wr->get_last_error();
            delete wr;
            break;
          }
          delete wr;
        }
        break;
      }
	  
	  
      // -------------------------------------  INTERFACE TO V1 CRYOCOOLER  --------------------------------------------------------
      //----------------------------------------------------------------------------------------------------------------------------
	  
      //- WRITE A REAL ----------------
    case HWProxy_ns::SET_REAL_MSG:
      {
        DEBUG_STREAM << "CryoCoolerInterface::process_message handling SET_REAL_MSG user msg" << std::endl;
        //- get msg data...
        struct HWProxy_ns::WReal * wr = 0;
        _msg.detach_data(wr);
        if (wr) 
        {	
          try
          {
            DEBUG_STREAM << "CryoCoolerInterface::process_message (1) trying to write REAL value " << wr->value << " at offset " << wr->byte_offset << std::endl;
            if (hwp_wr)
            {
              hwp_wr->write_real ("CryoCoolerInterface::process_message (1) trying to write REAL value",wr->byte_offset, wr->value);
              hwp_wr->write_bit ("CryoCoolerInterface::process_message (1) trying to write REAL value", ACTION_REQUEST_BYTE_OFFSET, ACTION_REQUEST_BIT_OFFSET, true);
            }
          }
          catch (...)
          {
            yat::AutoMutex<yat::Mutex> guard (this->m_com_lock);
            this->com_status = hwp_wr->get_com_status();
            this->com_state = hwp_wr->get_com_state();
            this->last_error = hwp_wr->get_last_error();
            delete wr;
            break;
          }
          delete wr;
        }
        break;
      }
	  	  
	  
      //- WRITE AN INT ----------------
    case HWProxy_ns::SET_INT_MSG:
      {
        DEBUG_STREAM << "CryoCoolerInterface::process_message handling SET_INT_MSG user msg" << std::endl;
        //- get msg data...
        struct HWProxy_ns::WInt * wi = 0;
        _msg.detach_data(wi);
        if (wi) 
        {
          try
          {
            DEBUG_STREAM << "CryoCoolerInterface::process_message (1) trying to write INT value " << wi->value << " at offset " << wi->byte_offset << std::endl;
            if (hwp_wr)
            {
              hwp_wr->write_short ("CryoCoolerInterface::process_message (1) trying to write INT value ", wi->byte_offset, wi->value);
              hwp_wr->write_bit ("CryoCoolerInterface::process_message (1) trying to write INT value ", ACTION_REQUEST_BYTE_OFFSET, ACTION_REQUEST_BIT_OFFSET, true);
            }
          }
          catch (...)
          {
            yat::AutoMutex<yat::Mutex> guard (this->m_com_lock);
            this->com_status = hwp_wr->get_com_status();
            this->com_state = hwp_wr->get_com_state();
            this->last_error = hwp_wr->get_last_error();
            delete wi;
            break;
          }
          delete wi;
        }
        break;
      }

      //- WRITE A BOOL ----------------
    case HWProxy_ns::SET_BOOL_MSG:
      {
        DEBUG_STREAM << "CryoCoolerInterface::process_message handling SET_BOOL_MSG user msg" << std::endl;
        //- get msg data...
        struct HWProxy_ns::WBool * wb = 0;
        _msg.detach_data(wb);
        if (wb) 
        {
          try
          {
            DEBUG_STREAM << "CryoCoolerInterface::process_message (1) trying to write BOOL value " << wb->value << " at offset " << wb->byte_offset << "." << wb->bit_offset << std::endl;
            if (hwp_wr)
            {
              hwp_wr->write_bit ("CryoCoolerInterface::process_message (1) trying to write BOOL value",wb->byte_offset, wb->bit_offset, wb->value);
              hwp_wr->write_bit ("CryoCoolerInterface::process_message (1) trying to write BOOL value",ACTION_REQUEST_BYTE_OFFSET, ACTION_REQUEST_BIT_OFFSET, true);
            }
          }
          catch (...)
          {
            yat::AutoMutex<yat::Mutex> guard (this->m_com_lock);
            this->com_status = hwp_wr->get_com_status();
            this->com_state = hwp_wr->get_com_state();
            this->last_error = hwp_wr->get_last_error();
            delete wb;
            break;
          }
          delete wb;
        }
        break;
      }
    } 
	
  } //- CryoCoolerInterface::process_message

  //-----------------------------------------------
  //- Acces to REAL -------------------------------
  //-----------------------------------------------
  float CryoCoolerInterface::get_real (unsigned int byte_offset)
  { 
    //DEBUG_STREAM << "CryoCoolerInterface::get_real get REAL value at offset " << byte_offset << std::endl;
    float fval = __NAN__;
	
    //- check max REAL offset
    unsigned int max_offset = 196;
    if (cryocooler_protocol.compare(V2_CC_PROTOCOL) == 0)
    { // V2
      max_offset = 200;
    }

    if (byte_offset > max_offset)
    {
      yat::AutoMutex<yat::Mutex> guard (this->m_com_lock);
      this->com_state = HWProxy_ns::HWP_SOFTWARE_ERROR;
      this->com_status = "HWP_SOFTWARE_ERROR";
      std::stringstream s;
      s << "CryoCoolerInterface::get_real Offset out of range trying to read a REAL " << " at offset " << byte_offset  << std::endl;
      this->last_error = s.str ();
      Tango::Except::throw_exception(  
            _CPTC ("OUT_OF_RANGE"),
            _CPTC (s.str().c_str()),
            _CPTC ("CryoCoolerInterface::get_real"));
    }

    if (!hwp) 
    {
      yat::AutoMutex<yat::Mutex> guard (this->m_com_lock);
      this->com_state = HWProxy_ns::HWP_SOFTWARE_ERROR;
      this->com_status = "HWP_SOFTWARE_ERROR";
      std::stringstream s;
      s << "CryoCoolerInterface::get_real last error occured trying to get hwp proxy instance " << std::endl;
      this->last_error = s.str ();
      Tango::Except::throw_exception (_CPTC ("OUT_OF_MEMORY"),
                          _CPTC (s.str().c_str()),
                          _CPTC ("CryoCoolerInterface::get_real ()"));
    }
    //- processing
    try
    {		
      if(hwp)
        fval =  hwp->get_real(byte_offset);
    }
    catch(...)
    {
      yat::AutoMutex<yat::Mutex> guard (this->m_com_lock);
      this->com_state = HWProxy_ns::HWP_SOFTWARE_ERROR;
      this->com_status = "HWP_SOFTWARE_ERROR";
      std::stringstream s;
      s << "CryoCoolerInterface::get_real last error occured trying to read a REAL " << " at offset " << byte_offset  << std::endl;
      this->last_error = s.str ();
      Tango::Except::throw_exception (_CPTC ("OUT_OF_MEMORY"),
                              _CPTC (s.str().c_str()),
                              _CPTC ("CryoCoolerInterface::get_real ()"));
    }

    return fval;
  }

  //-----------------------------------------------
  //- Acces to INT --------------------------------
  //-----------------------------------------------
  short CryoCoolerInterface::get_int (unsigned int byte_offset)
  {
    //DEBUG_STREAM << "CryoCoolerInterface::get_int get INT value at offset " << byte_offset << std::endl;
    short shval = -1;

    //- Check max INT offset
    unsigned int max_offset = 198;
    if (cryocooler_protocol.compare(V2_CC_PROTOCOL) == 0)
    { // V2
      max_offset = 200;
    }

    if (byte_offset > max_offset)
    {
      yat::AutoMutex<yat::Mutex> guard (this->m_com_lock);
      this->com_state = HWProxy_ns::HWP_SOFTWARE_ERROR;
      this->com_status = "HWP_SOFTWARE_ERROR";
      std::stringstream s;
      s << "CryoCoolerInterface::get_int Offset out of range trying to read an INT " << " at offset " << byte_offset  << std::endl;
      this->last_error = s.str ();
      Tango::Except::throw_exception(  
            _CPTC ("OUT_OF_RANGE"),
            _CPTC (s.str().c_str()),
            _CPTC ("CryoCoolerInterface::get_int "));
    }
	
    if (!hwp)
    {
      yat::AutoMutex<yat::Mutex> guard (this->m_com_lock);
      this->com_state = HWProxy_ns::HWP_SOFTWARE_ERROR;
      this->com_status = "HWP_SOFTWARE_ERROR";
      std::stringstream s;
      s << "CryoCoolerInterface::get_int last error occured trying to get hwp proxy instance " << std::endl;
      this->last_error = s.str ();
      Tango::Except::throw_exception (_CPTC ("OUT_OF_MEMORY"),
                            _CPTC (s.str().c_str()),
                            _CPTC ("CryoCoolerInterface::get_int ()"));
    }
	
    //- processing
    try
    {
      if (hwp)
        shval =  hwp->get_word(byte_offset);
    }
    catch(...)
    {
      yat::AutoMutex<yat::Mutex> guard (this->m_com_lock);
      this->com_state = HWProxy_ns::HWP_SOFTWARE_ERROR;
      this->com_status = "HWP_SOFTWARE_ERROR";
      std::stringstream s;
      s << "CryoCoolerInterface::get_int last error occured trying to read an INT " << " at offset " << byte_offset  << std::endl;
      this->last_error = s.str ();
      Tango::Except::throw_exception (_CPTC ("OUT_OF_MEMORY"),
                            _CPTC (s.str().c_str()),
                            _CPTC ("CryoCoolerInterface::get_int ()"));
    }

    return shval;	
  }

  //-----------------------------------------------
  //- Acces to BOOL -------------------------------
  //-----------------------------------------------
  bool CryoCoolerInterface::get_bool (unsigned int bit_offset, unsigned  int byte_offset)
  {
    //DEBUG_STREAM << "CryoCoolerInterface::get_bool trying to get BOOL value at offset " << byte_offset << "." << bit_offset << std::endl;
    
    bool bval = false;

    //- Check max BOOL offset
    unsigned int max_offset = 199;
    if (cryocooler_protocol.compare(V2_CC_PROTOCOL) == 0)
    { // V2
      max_offset = 200;
    }

    if (byte_offset > max_offset)
    {
      yat::AutoMutex<yat::Mutex> guard (this->m_com_lock);
      this->com_state = HWProxy_ns::HWP_SOFTWARE_ERROR;
      this->com_status = "HWP_SOFTWARE_ERROR";
      std::stringstream s;
      s << "CryoCoolerInterface::get_bool Offset out of range trying to read a BOOL " << " at offset " << byte_offset << "." << bit_offset  << std::endl;
      this->last_error = s.str ();
      Tango::Except::throw_exception(  
            _CPTC ("OUT_OF_RANGE"),
            _CPTC (s.str().c_str()),
            _CPTC ("CryoCoolerInterface::get_bool"));
    }	

    if (!hwp)
    {
      yat::AutoMutex<yat::Mutex> guard (this->m_com_lock);
      this->com_state = HWProxy_ns::HWP_SOFTWARE_ERROR;
      this->com_status = "HWP_SOFTWARE_ERROR";
      std::stringstream s;
      s << "CryoCoolerInterface::get_bool last error occured trying to get hwp proxy instance " << std::endl;		
      this->last_error = s.str ();
      Tango::Except::throw_exception (_CPTC ("OUT_OF_MEMORY"),
                        _CPTC (s.str().c_str()),
                        _CPTC ("CryoCoolerInterface::get_bool ()"));
    }
	
    //- processing
    try
    {		
      if(hwp)
        bval =  hwp->get_bool(byte_offset, bit_offset);
    }
    catch(...)
    {
      yat::AutoMutex<yat::Mutex> guard (this->m_com_lock);
      this->com_state = HWProxy_ns::HWP_SOFTWARE_ERROR;
      this->com_status = "HWP_SOFTWARE_ERROR";
      std::stringstream s;
      s << "CryoCoolerInterface::get_bool last error occured trying to read a  BOOL " << " at offset " << byte_offset  << "."<<bit_offset<<std::endl;
      Tango::Except::throw_exception (_CPTC ("OUT_OF_MEMORY"),
                                _CPTC (s.str().c_str()),
                                _CPTC ("CryoCoolerInterface::get_bool ()"));
    }

    return bval;		
  }

  //-----------------------------------------------
  //- Acces to HW ---------------------------------
  //-----------------------------------------------
  void CryoCoolerInterface::periodic_job_i (void)
  {
    yat::AutoMutex<yat::Mutex> guard (this->m_com_lock);
    long l_com_error = 0;
    long l_com_success = 0;

    if (hwp)
    {
      l_com_success += hwp->get_com_success();
      l_com_error += hwp->get_com_error();
      this->com_state = hwp->get_com_state();

      if (com_state == HWProxy_ns::HWP_COMMUNICATION_ERROR)
      {
        this->com_status = hwp->get_com_status();
        this->last_error = hwp->get_last_error();
        return;
      }
    }

    //- hwp_wr is alive	
    if (hwp_wr)
    {
      l_com_success += hwp_wr->get_com_success();
      l_com_error += hwp_wr->get_com_error();
      this->com_state = hwp_wr->get_com_state();

      if (com_state == HWProxy_ns::HWP_COMMUNICATION_ERROR)
      {
        this->com_status = hwp_wr->get_com_status();
        this->last_error = hwp_wr->get_last_error();
        return;
      }
    }
    this->com_success = l_com_success;
    this->com_error = l_com_error; 
  }

} //- namespace
