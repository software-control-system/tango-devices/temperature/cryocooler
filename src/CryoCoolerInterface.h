
//- Project : Cryocooler, ACCEL/CRYOTHERM cryogenic cooler for optics at SOLEIL
//- file : CryoCoolerIneterface.h
//- threaded reading of the PLC Data Block
//- Writes on the PLC


#ifndef __CRYOCOOLER_INTERFACE_H__
#define __CRYOCOOLER_INTERFACE_H__

#include "TypesAndConsts.h"
#include <yat4tango/DeviceTask.h>
#include <yat/time/Timer.h>
#include <DeviceProxyHelper.h>
#include <PLCServerProxy.h>
#include <HWProxy.h>



namespace CryoCooler_ns
{
 const size_t SET_COMMAND_MSG      = yat::FIRST_USER_MSG + 200;
 const size_t SET_REAL_V2_MSG      = yat::FIRST_USER_MSG + 201;

  // ============================================================================
  // some defines enums and constants
  // ============================================================================
//- some define and constants

  //------------------------------------------------------------------------
  //- CryoCoolerIneterface Class
  //- read /write in the PLC with the help of HWProxy class
  //------------------------------------------------------------------------
  class CryoCoolerInterface : public yat4tango::DeviceTask
  {
    public :
	
      //- Constructeur/destructeur
      CryoCoolerInterface (Tango::DeviceImpl * _host_device, 
			std::string _plc_name,
			short _dbread,
			short _dbwrite,
			size_t _periodic_exec_ms,
			std::string _cryoCoolerProtocol);
              
      virtual ~CryoCoolerInterface ();

      //- Initialize all DB -----------------------
      void initialize ()
        throw(Tango::DevFailed);
		
      //- retrieve the data from the PLC
      //- we write in the PLC thanks to the task message service
      float get_real (unsigned int byte_offset);
      short get_int  (unsigned int byte_offset);
      bool get_bool  (unsigned int bit_offset, unsigned  int byte_offset);
		
      //- the last error string
      std::string get_last_error (void) {	return last_error;}
      //- the number of communication errors
      unsigned long get_com_error (void) { return com_error;}
      //- the number of communication successfully done
      unsigned long get_com_success (void){ return com_success;}
      //- the state and status of communication
      HWProxy_ns::ComState get_com_state (void){ return com_state;}
      std::string get_com_status (void){ return com_status;}
		
      //- the cryocooler protocol version:
      std::string get_protocol_version() { return cryocooler_protocol;}		
		
    protected:
      //- process_message (implements yat4tango::DeviceTask pure virtual method)
      virtual void process_message (yat::Message& msg) throw (Tango::DevFailed);
		
		
    private :
	
    //- members
    //------------------------------------------
    //- the host device 
    Tango::DeviceImpl * host_dev;
    std::string plc_name;
    short db_read_number;
    short db_write_number;
    size_t periodic_timeout_ms;
	
    HWProxy_ns::HWProxy * hwp;
    //- DB write hwp proxy
    HWProxy_ns::HWProxy * hwp_wr;
	
    //- the state and status stuff
    HWProxy_ns::ComState com_state;
    std::string com_status;
	
    //- last error
    std::string last_error;
    //- for CryoCooler Com error attribute
    unsigned long com_error;
    //- for CryoCooler Com OK attribute
    unsigned long com_success;

    // cryocooler protocol version
    std::string cryocooler_protocol;
	  
    //- periodic job
    void periodic_job_i (void);

    //- mutex for communication data protection
    yat::Mutex m_com_lock;
  };

}
#endif //- __CRYOCOOLER_INTERFACE_H__
