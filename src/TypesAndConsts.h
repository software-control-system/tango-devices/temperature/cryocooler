//=============================================================================
// file :        TypesAndConsts.h
//
// description : some types and consts
//               
// project :	CryoCooler
//
//=============================================================================
#ifndef __TYPES_AND_CONSTS_H__
#define __TYPES_AND_CONSTS_H__

#include <yat/utils/String.h>

namespace CryoCooler_ns
{

  // ============================================================================
  // some defines enums and constants
  // ============================================================================
  static const float __NAN__ = ::sqrt(float(-1.));
  
  //- constantes descriptions
  //- DB_START constantes
  static unsigned int DB_START_INPUT_OFFSET  = 0;
  static unsigned int DB_START_INPUT_LEN     = 50;
  static unsigned int DB_START_OUTPUT_OFFSET = 0;
  static unsigned int DB_START_OUTPUT_LEN    = 0;

  //- DB_END constantes
  static unsigned int DB_END_INPUT_OFFSET  = 100;
  static unsigned int DB_END_INPUT_OFFSET_V2  = 101; // DB read length for V2 protocol
  static unsigned int DB_END_INPUT_LEN     = 50;
  static unsigned int DB_END_OUTPUT_OFFSET = 0;
  static unsigned int DB_END_OUTPUT_LEN    = 0;

  //- DB_WRITE constantes
  static unsigned int DB_WRITE_INPUT_OFFSET  = 0;
  
  // -------------------------------------------
  // - WRITE must be = READ_WRITE.
  // At least one element in input zone otherwise
  // the read_hard () method raises an exception.
  // Pb corrected in PLCServerProxy version 3.0.5
  // ----------------------------------------------
  static unsigned int DB_WRITE_INPUT_LEN     = 1; 
  static unsigned int DB_WRITE_INPUT_LEN_V2  = 0; // no BD read for V2 protocol
  static unsigned int DB_WRITE_OUTPUT_OFFSET = 0;
  static unsigned int DB_WRITE_OUTPUT_LEN    = 100;
  static unsigned int DB_WRITE_OUTPUT_LEN_V2 = 3; // DB write length for V2 protocol

  // ---------------------------------------------
  // - CryoCooler protocol versions: 
  static std::string V1_CC_PROTOCOL = "V1"; // "CPU313C-2DP + CPU343" protocol
  static std::string V2_CC_PROTOCOL = "V2"; // "CPU IM151-8" protocol

} //- namespace

#endif //- __TYPES_AND_CONSTS_H__


