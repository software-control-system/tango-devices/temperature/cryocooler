//- file : Parts.cpp
//- classes handling the parts of the CryoCooler

#include <Parts.h>
#include <yat4tango/ExceptionHelper.h>
#include <DBOffsets.h>


namespace CryoCooler_ns
{

  //---------------------------------------------------------------------
  //- Part Class Implementation------------------------------------------
  //---------------------------------------------------------------------

  //- CTOR -----------------------------------------------
  Part::Part (Tango::DeviceImpl * _host_device)
              : Tango::LogAdapter (_host_device)
  {
    DEBUG_STREAM << " Part::Part <- " << std::endl;
    //- HWProxy instanciation (Cryocooler class)
    this->com_status.resize (512);

    cryo_itf = CryoCoolerCom::get_cryo_itf();

    // get cryocooler version and check if V1 or V2
    std::string tmp_protocol = cryo_itf->get_protocol_version();
    if (tmp_protocol.compare(V2_CC_PROTOCOL) == 0)
      this->is_protocol_V2 = true;
    else
      this->is_protocol_V2 = false;
	
    this->com_state = HWProxy_ns::HWP_NO_ERROR;
    this->com_status = std::string("communication : initializing") 
      + std::string(" with protocol ")
      + tmp_protocol;
  }

  //- DTOR --------------------------------------------
  Part::~Part (void)
  {
    DEBUG_STREAM << " Part::~Part <- " << std::endl;
    //- noop Dtor
  }

  //- Get Com State -----------------------------------
  HWProxy_ns::ComState Part::get_com_state (void)
  {
    //DEBUG_STREAM << " Part::get_com_state <- " << std::endl;
    if (cryo_itf.get())
      this->com_state = cryo_itf.get()->get_com_state ();
    else 
    {
      this->com_status += "\nlast error : Part::get_com_state() cannot get CryoCooler interface pointer  ...";
      this->com_state = HWProxy_ns::HWP_COMMUNICATION_ERROR;
    }
		
    return com_state;
  }

  //- Get Com Status ----------------------------------
  std::string Part::get_com_status (void)
  {
    //DEBUG_STREAM << " Part::get_com_status <- " << std::endl;
    if (cryo_itf.get())
    {
      this->com_status =  cryo_itf.get()->get_com_status ();
      this->com_status += "\nlast error : " + this->cryo_itf.get()->get_last_error ();
    }
    else 
      this->com_status = "Part::get_com_state() cannot get CryoCooler interface pointer  ...";
    
    return com_status;
  }

  //- get a REAL (siemens PLC float 32 bits value)
  //- must get 2 consecutive WORDS (siemens unsigned 16 bits value)
  //- GetReal -----------------------------------------
  float Part::GetReal (unsigned int offset)
  {
    //DEBUG_STREAM << " Part::GetReal <- " << std::endl;
    float tmp = __NAN__;
    try 
    {
      if(cryo_itf.get())
        tmp = cryo_itf.get()->get_real (offset);
    }
    catch (...)
    {
      this->get_com_state ();
      this->get_com_status ();
    }

    return tmp;
  }

  //- GetInt ------------------------------------------
  //- get a INT (siemens PLC 16 bit signed value)
  short Part::GetInt (unsigned int offset)
  {
    //DEBUG_STREAM << " Part::GetInt <- " << std::endl;
    short tmp = -1;
    try
    {
      if(cryo_itf.get())
        tmp = cryo_itf->get_int (offset);
    }
    catch (...)
    {
      this->get_com_state ();
      this->get_com_status ();
    }

    return tmp;
  }

 //- GetBool ------------------------------------------
 //- get a bool get_com_status
  bool Part::GetBool (unsigned int bit_offset, unsigned int byte_offset)
  {
    //DEBUG_STREAM << " Part::GetBool <- " << std::endl;
    //- PLCServer wants Byte offset pair and bit offset 0...15
    if ((byte_offset & 0x01) != 0)
    {
      byte_offset--;
      bit_offset += 8;
    }

    bool tmp = false;
    try
    {
      if(cryo_itf.get())
        tmp = cryo_itf.get()->get_bool (bit_offset, byte_offset);
    }
    catch (...)
    {
      this->get_com_state ();
      this->get_com_status ();
    }

    return tmp;
  }

  //- SetReal -----------------------------------------
  //- set a REAL
  void Part::SetReal (unsigned int offset, float v)
  {
    DEBUG_STREAM << " Part::SetReal <- " << std::endl;
    
    yat::Message * msg = NULL;

    // Manage V2 protocol =====>
    if (!is_protocol_V2)
      msg = new yat::Message(HWProxy_ns::SET_REAL_MSG); // V1
    else
      msg = new yat::Message(CryoCooler_ns::SET_REAL_V2_MSG); // V2

    if (msg == 0)
    {
      ERROR_STREAM << "Part::SetReal error trying to create msg "<< endl;
      Tango::Except::throw_exception (_CPTC ("OUT_OF_MEMORY"),
                                      _CPTC ("yat::Message allocation failed"),
                                      _CPTC ("Part::SetReal"));
    }
    struct HWProxy_ns::WReal wr;
    wr.byte_offset = offset;
    wr.value = v;
    msg->attach_data(wr);
    this->cryo_itf.get()->post(msg);
  }

  //- SetBool -----------------------------------------
  //- set a bool
  void Part::SetBool (unsigned int bit_offset, unsigned int byte_offset, bool v)
  {
    DEBUG_STREAM << " Part::SetBool <- " << std::endl;
    yat::Message * msg = new yat::Message(HWProxy_ns::SET_BOOL_MSG);
    if (msg == 0)
    {
      ERROR_STREAM << "Part::SetBool error trying to create msg "<< endl;
      Tango::Except::throw_exception (_CPTC ("OUT_OF_MEMORY"),
                                      _CPTC ("yat::Message allocation failed"),
                                      _CPTC ("Part::SetBool"));
    }
    struct HWProxy_ns::WBool wb;
    //- PLCServer wants Byte offset pair and bit offset 0...15
    if ((byte_offset & 0x01) != 0)
    {
      byte_offset--;
      bit_offset += 8;
    }
      
    wb.byte_offset = byte_offset;
    wb.bit_offset = bit_offset;
    wb.value = v;
    msg->attach_data(wb);
    this->cryo_itf.get()->post(msg);
  }

  //- SendCmd -----------------------------------------
  //- send a command
  void Part::SendCmd (unsigned int offset)
  {
    DEBUG_STREAM << " Part::SendCmd <- " << std::endl;
    
    yat::Message * msg = new yat::Message(CryoCooler_ns::SET_COMMAND_MSG);

    if (msg == 0)
    {
      ERROR_STREAM << "Part::SendCmd error trying to create msg "<< endl;
      Tango::Except::throw_exception (_CPTC ("OUT_OF_MEMORY"),
                                      _CPTC ("yat::Message allocation failed"),
                                      _CPTC ("Part::SendCmd"));
    }
    struct HWProxy_ns::WInt wi;
    wi.byte_offset = offset;
    wi.value = 0;
    msg->attach_data(wi);
    this->cryo_itf.get()->post(msg);
  }

  // ============================================================================
  //- pseudo singleton
  // ============================================================================
  yat::SharedPtr<CryoCooler_ns::CryoCoolerInterface> CryoCoolerCom::cryo_itf;

  //---------------------------------------------------------------------
  //- Cryocooler Class Implementation
  //---------------------------------------------------------------------

  //- CTor ------------------------------------------------
  CryoCoolerCom::CryoCoolerCom (size_t _periodic_exec_ms, 
              Tango::DeviceImpl * _devimpl,
              std::string _plc_name,
              short _dbread,
              short _dbwrite,
              std::string _cryoCoolerProtocol)
              : Tango::LogAdapter (_devimpl),
                period (_periodic_exec_ms),
                devimpl (_devimpl),
                plc_name (_plc_name),
                db_read_number (_dbread),
                db_write_number (_dbwrite),
                stop_on_byte_offset (STOP_ON_BYTE_OFFSET),
                stop_on_bit_offset (STOP_ON_BIT_OFFSET),
                stop_button_byte_offset (STOP_BUTTON_BYTE_OFFSET),
                stop_button_bit_offset (STOP_BUTTON_BIT_OFFSET),
                no_alarm_byte_offset (CC_NO_ALARM_BYTE_OFFSET),
                no_alarm_bit_offset (CC_NO_ALARM_BIT_OFFSET),
                enabled_byte_offset (CC_ENABLE_BYTE_OFFSET),
                enabled_bit_offset (CC_ENABLE_BIT_OFFSET),
                ready_byte_offset (CC_READY_BYTE_OFFSET),
                ready_bit_offset (CC_READY_BIT_OFFSET),
                ups_no_fail_byte_offset (0),
                ups_no_fail_bit_offset (0),	
                sensor_conn_byte_offset (0),
                sensor_conn_bit_offset (0),	
                valve_conn_byte_offset (0),
                valve_conn_bit_offset (0),
				heater_conn_byte_offset (0),
				heater_conn_bit_offset (0),
                cryocooler_protocol(_cryoCoolerProtocol)
  {
    DEBUG_STREAM << "CryoCoolerCom::CryoCoolerCom entering..." << std::endl;

    // V2 Management
    if (cryocooler_protocol.compare(V2_CC_PROTOCOL) == 0)
    { // V2
      stop_button_byte_offset = CC_OFF;
      stop_button_bit_offset = 0;  
      ups_no_fail_byte_offset = UPS_NO_POWER_FAIL_BYTE_OFFSET;
      ups_no_fail_bit_offset = UPS_NO_POWER_FAIL_BIT_OFFSET;	
      sensor_conn_byte_offset = SENSOR_CABLE_CONN_OK_BYTE_OFFSET;
      sensor_conn_bit_offset = SENSOR_CABLE_CONN_OK_BIT_OFFSET;	
      valve_conn_byte_offset = VALVE_CABLE_CONN_OK_BYTE_OFFSET;
      valve_conn_bit_offset = VALVE_CABLE_CONN_OK_BIT_OFFSET;
	  heater_conn_byte_offset = HEATER_CABLE_CONN_OK_BYTE_OFFSET;
      heater_conn_bit_offset = HEATER_CABLE_CONN_OK_BIT_OFFSET;
    }
  }

  //- DTor ----------------------------------------------
  CryoCoolerCom::~CryoCoolerCom ()
  {
    DEBUG_STREAM << "CryoCoolerCom::~CryoCoolerCom entering..." << std::endl;
    cryo_itf.reset();
  }

  //- get the HWProxy instance ------------------------
  yat::SharedPtr<CryoCoolerInterface> CryoCoolerCom::get_cryo_itf (void)
  {
    std::cout << "CryoCoolerCom::get_cryo_itf entering..." << std::endl;
    if (! cryo_itf.get())
    {
      std::cout << "CryoCoolerCom::get_cryo_itf could not get HWProxy ..." << std::endl;
      THROW_DEVFAILED(_CPTC("SOFTWARE_ERROR"),
                      _CPTC("unexpected NULL pointer [The cryo_itf not properly initialized]"),
                      _CPTC("CryoCoolerCom::get_cryo_itf")); 

    }
    return cryo_itf;
  }

  //- We must initialise outside of Ctor --------------
  void CryoCoolerCom::init (void)
  {
    DEBUG_STREAM << "CryoCoolerCom::init entering..." << std::endl;
    if (cryo_itf.get())
      return;

    try 
    {
      DEBUG_STREAM << "CryoCoolerCom::init trying to create HWProxy" << std::endl;

      cryo_itf.reset(new CryoCoolerInterface (devimpl,plc_name, db_read_number,db_write_number,period,cryocooler_protocol), yat4tango::DeviceTaskExiter());

      if (!cryo_itf.get())
      {
        ERROR_STREAM << "CryoCoolerCom::init could not instanciate CryoCooler Interface ..." << std::endl;
        throw std::bad_alloc();
      }
	  
      //- Initialize hw proxy for DB_start, DB_end, DB_write
      cryo_itf.get()->initialize();
	 
      //- go for CryoInterfaceS
      cryo_itf.get()->go ();
    }
    catch (std::bad_alloc)
    {
      FATAL_STREAM << "CryoCoolerCom::init () catched bad_alloc" << std::endl;
      this->com_state = HWProxy_ns::HWP_INITIALIZATION_ERROR;
      this->last_error = "could not instanciate HWProxy class catched bad_alloc";
      return;
    }
    catch (...)
    {
      FATAL_STREAM << "CryoCoolerCom::init () catched bad_alloc" << std::endl;
      this->com_state = HWProxy_ns::HWP_INITIALIZATION_ERROR;
      this->last_error = "could not instanciate HWProxy class catched (...)";
      return;
    }
  }

  //- Communication health ---------------------------
  unsigned long CryoCoolerCom::ComSuccess (void) 
  { 
    //DEBUG_STREAM << "CryoCoolerCom::ComSuccess entering..." << std::endl;
    return cryo_itf.get()->get_com_success ();
  }

  unsigned long CryoCoolerCom::ComError (void) 
  { 
    //DEBUG_STREAM << "CryoCoolerCom::ComError entering..." << std::endl;
    return cryo_itf.get()->get_com_error ();
  }

  HWProxy_ns::ComState CryoCoolerCom::GetComState (void) 
  {
    //DEBUG_STREAM << "CryoCoolerCom::ComState entering..." << std::endl;
    return cryo_itf.get()->get_com_state ();
  }

  std::string CryoCoolerCom::GetComStatus (void) 
  {
    //DEBUG_STREAM << "CryoCoolerCom::ComStatus entering..." << std::endl;
    return (cryo_itf.get()->get_com_status () + "\n" + cryo_itf.get()->get_last_error ());
  }

  //- Stop On Get ---------------------------
  bool CryoCoolerCom::StopOn (void)
  {
    DEBUG_STREAM << "CryoCoolerCom::StopOn (read data) entering..." << std::endl;
    bool tmp = cryo_itf.get()->get_bool (stop_on_bit_offset, stop_on_byte_offset);
    return tmp;
  }

  //- Stop On Set ---------------------------
  void CryoCoolerCom::StopButton (bool v)
  {
    DEBUG_STREAM << "CryoCoolerCom::StopButton (write data) entering..." << std::endl;

    yat::Message * msg = NULL;
    // Manage V2 protocol
    if (cryocooler_protocol.compare(V1_CC_PROTOCOL) == 0)
    { // V1
      msg = new yat::Message(HWProxy_ns::SET_BOOL_MSG);
      if (msg == 0)
      {
        ERROR_STREAM << "CryoCoolerCom::StopButton error trying to create msg "<< endl;
        Tango::Except::throw_exception (_CPTC ("OUT_OF_MEMORY"),
                                        _CPTC ("yat::Message allocation failed"),
                                        _CPTC ("CryoCoolerCom::StopButton"));
      }

      struct HWProxy_ns::WBool wb;
      wb.byte_offset = stop_button_byte_offset;
      wb.bit_offset = stop_button_bit_offset;
      wb.value = v;
      msg->attach_data(wb);
    }
    else
    { // V2
      msg = new yat::Message(CryoCooler_ns::SET_COMMAND_MSG);

      if (msg == 0)
      {
        ERROR_STREAM << "CryoCoolerCom::StopButton error trying to create msg "<< endl;
        Tango::Except::throw_exception (_CPTC ("OUT_OF_MEMORY"),
                                        _CPTC ("yat::Message allocation failed"),
                                        _CPTC ("CryoCoolerCom::StopButton"));
      }

      struct HWProxy_ns::WInt wi;
      wi.byte_offset = stop_button_byte_offset;
      wi.value = 0;
      msg->attach_data(wi);
    }

    this->cryo_itf->post(msg);
  }

  //- Get NO ALARM ------------------------------
  bool CryoCoolerCom::NoAlarm (void)
  {
    //DEBUG_STREAM << "CryoCoolerCom::NoAlarm read data entering..." << std::endl;
    bool tmp = cryo_itf.get()->get_bool (no_alarm_bit_offset, no_alarm_byte_offset);
    return tmp;
  }

  //- Get ENABLED -------------------------------
  bool CryoCoolerCom::Enabled (void)
  {
    //DEBUG_STREAM << "CryoCoolerCom::Enabled read data entering..." << std::endl;
    bool tmp = cryo_itf.get()->get_bool (enabled_bit_offset, enabled_byte_offset);
    return tmp;
  }

  //- Get READY ---------------------------------
  bool CryoCoolerCom::Ready (void)
  {
    //DEBUG_STREAM << "CryoCoolerCom::Ready read data entering..." << std::endl;
    bool tmp = cryo_itf.get()->get_bool (ready_bit_offset, ready_byte_offset);
    return tmp;
  }

  //- Get UPS NO POWER FAIL ---------------------------------
  bool CryoCoolerCom::UpsNoPowerFail (void)
  {
    //DEBUG_STREAM << "CryoCoolerCom::UpsNoPowerFail read data entering..." << std::endl;
    bool tmp = cryo_itf.get()->get_bool (ups_no_fail_bit_offset, ups_no_fail_byte_offset);
    return tmp;
  }
  
  //- Get SENSOR CABLE CONNECTED ---------------------------------
  bool CryoCoolerCom::SensorCableConnected (void)
  {
    //DEBUG_STREAM << "CryoCoolerCom::SensorCableConnected read data entering..." << std::endl;
    bool tmp = cryo_itf.get()->get_bool (sensor_conn_bit_offset, sensor_conn_byte_offset);
    return tmp;
  }
  //- Get VALVE CABLE CONNECTED ---------------------------------
  bool CryoCoolerCom::ValveCableConnected (void)
  {
    //DEBUG_STREAM << "CryoCoolerCom::ValveCableConnected read data entering..." << std::endl;
    bool tmp = cryo_itf.get()->get_bool (valve_conn_bit_offset, valve_conn_byte_offset);
    return tmp;
  }
  //- Get HEATER CABLE CONNECTED ---------------------------------
  bool CryoCoolerCom::HeaterCableConnected (void)
  {
    //DEBUG_STREAM << "CryoCoolerCom::HeaterCableConnected read data entering..." << std::endl;
    bool tmp = cryo_itf.get()->get_bool (heater_conn_bit_offset, heater_conn_byte_offset);
    return tmp;
  }  

  //---------------------------------------------------------------------
  //- DigitalValve Class Implementation
  //---------------------------------------------------------------------
  DigitalValve::DigitalValve (Tango::DeviceImpl * _host_device,
                              std::string _name) : 
                                    Part (_host_device),
                                    name (_name),
                                    setup_error (false)
  {
    DEBUG_STREAM << "DigitalValve::DigitalValve buiding "
                 << this->name 
                 << std::endl;
    //- legal names : [V9|V15|V17.1|V17.2|V19|V20|V21|VentPipe
    status = "initializing...";
	
    if (name == "V9")
    {
      man_on_bit_offset   = VALV_MAN_ON_V9_BIT_OFFSET;
      man_on_byte_offset  = VALV_MAN_ON_V9_BYTE_OFFSET;
      man_off_bit_offset  = VALV_MAN_OFF_V9_BIT_OFFSET;
      man_off_byte_offset = VALV_MAN_OFF_V9_BYTE_OFFSET;
      status_byte_offset  = Q_OP_STATE_V9_OFFSET;

      // Manage V2 protocol =====>
      if (is_protocol_V2)
      {
        man_on_bit_offset   = 0;
        man_on_byte_offset  = OPEN_V9;
        man_off_bit_offset  = 0;
        man_off_byte_offset = CLOSE_V9;
      }
      // <=====
    }
    else if (name == "V15")
    {
      man_on_bit_offset   = VALV_MAN_ON_V15_BIT_OFFSET;
      man_on_byte_offset  = VALV_MAN_ON_V15_BYTE_OFFSET;
      man_off_bit_offset  = VALV_MAN_OFF_V15_BIT_OFFSET;
      man_off_byte_offset = VALV_MAN_OFF_V15_BYTE_OFFSET;
      status_byte_offset  = Q_OP_STATE_V15_OFFSET;

      // Manage V2 protocol =====>
      if (is_protocol_V2)
      {
        man_on_bit_offset   = 0;
        man_on_byte_offset  = OPEN_V15;
        man_off_bit_offset  = 0;
        man_off_byte_offset = CLOSE_V15;
      }
      // <=====
    }
    else if (name == "V19")
    {
      man_on_bit_offset    = 0;
      man_on_byte_offset  = 0;
      man_off_bit_offset  = 0;
      man_off_byte_offset = 0;
      status_byte_offset  = Q_OP_STATE_V19_OFFSET;

      // Manage V2 protocol =====>
      if (is_protocol_V2)
      { // V19 is an analog valve in V2 protocol
        this->setup_error = true;
        this->state = -1;
        status = name + " Name Error\n -- In V2 protocol, V19 valve is an analog valve (see V19A)!";	    
      }
      // <=====
    }
    else if (name == "V20")
    {
      man_on_bit_offset   = VALV_MAN_ON_V20_BIT_OFFSET;
      man_on_byte_offset  = VALV_MAN_ON_V20_BYTE_OFFSET;
      man_off_bit_offset  = VALV_MAN_OFF_V20_BIT_OFFSET;
      man_off_byte_offset = VALV_MAN_OFF_V20_BYTE_OFFSET;
      status_byte_offset  = Q_OP_STATE_V20_OFFSET;

      // Manage V2 protocol =====>
      if (is_protocol_V2)
      {
        man_on_bit_offset   = 0;
        man_on_byte_offset  = OPEN_V20;
        man_off_bit_offset  = 0;
        man_off_byte_offset = CLOSE_V20;
      }
      // <=====
    }
    else if (name == "V21")
    {
      man_on_bit_offset   = VALV_MAN_ON_V21_BIT_OFFSET;
      man_on_byte_offset  = VALV_MAN_ON_V21_BYTE_OFFSET;
      man_off_bit_offset  = VALV_MAN_OFF_V21_BIT_OFFSET;
      man_off_byte_offset = VALV_MAN_OFF_V21_BYTE_OFFSET;
      status_byte_offset  = Q_OP_STATE_V21_OFFSET;

      // Manage V2 protocol =====>
      if (is_protocol_V2)
      {
        man_on_bit_offset   = 0;
        man_on_byte_offset  = OPEN_V21;
        man_off_bit_offset  = 0;
        man_off_byte_offset = CLOSE_V21;
      }
      // <=====
    }
    else if (name == "VentPipe")
    {
      man_on_bit_offset   = VENT_PIPE_MAN_ON_BIT_OFFSET;
      man_on_byte_offset  = VENT_PIPE_MAN_ON_BYTE_OFFSET;
      man_off_bit_offset  = VENT_PIPE_MAN_OFF_BIT_OFFSET;
      man_off_byte_offset = VENT_PIPE_MAN_OFF_BYTE_OFFSET;
      status_byte_offset  = Q_OP_STATE_VENT_PIPE_OFFSET;

      // Manage V2 protocol =====>
      if (is_protocol_V2)
      {
        man_on_bit_offset   = 0;
        man_on_byte_offset  = VP_HEAT_ON;
        man_off_bit_offset  = 0;
        man_off_byte_offset = VP_HEAT_OFF;
      }
      // <=====
    }
    else if (name == "V17.1")
    {
      man_on_bit_offset   = 0;
      man_on_byte_offset  = OPEN_V17_100;
      man_off_bit_offset  = 0;
      man_off_byte_offset = CLOSE_V17_100;
      status_byte_offset  = Q_OP_STATE_V17_100_OFFSET_V2;
	  
      // Manage V2 protocol =====>
      if (!is_protocol_V2)
      { // not defined in V1 protocol
        this->setup_error = true;
        this->state = -1;
        status = name + " Name Error\n -- In V1 protocol, V17.1 valve is not defined!";	  
      }
      // <=====		
    }	
    else if (name == "V17.2")
    {
      man_on_bit_offset   = 0;
      man_on_byte_offset  = OPEN_V17_35;
      man_off_bit_offset  = 0;
      man_off_byte_offset = CLOSE_V17_35;
      status_byte_offset  = Q_OP_STATE_V17_35_OFFSET_V2;
	  
      // Manage V2 protocol =====>
      if (!is_protocol_V2)
      { // not defined in V1 protocol
        this->setup_error = true;
        this->state = -1;
        status = name + " Name Error\n -- In V1 protocol, V17.2 valve is not defined!";	
      }
      // <=====			
	}
    //- error : must throw exception at runtime
    else
    {
      this->setup_error = true;
      this->state = -1;
      status = name + " Name Error\n -- Legal Names are [V9|V15|V17.1|V17.2|V19|V20|V21|VentPipe]";
    }
  }

  //- Dtor ------------------------------
  DigitalValve::~DigitalValve (void)
  {
    //- noop DTor
    DEBUG_STREAM << " DigitalValve::~DigitalValve deleting "
                 << this->name 
                 << std::endl;
  }

  //- Open ------------------------------
  void DigitalValve::Open ()
  {
    DEBUG_STREAM  << " DigitalValve::Open Digital Valve " 
                  << this->name 
                  << " ValvManOn address " 
                  << this->man_on_byte_offset
                  << "."
                  << this->man_on_bit_offset
                  << std::endl;

    if (this->setup_error)
      THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"),
                      _CPTC("invalid valve name -- Legal Names are [V9|V15|V17.1|V17.2|V19|V20|V21|VentPipe]"),
                      _CPTC("DigitalValve::Open")); 

    //- special case : SubCooler filling
    if (name == "V19")
    {
      // Manage V2 protocol
      if (!is_protocol_V2)
      { // V1
        SetBool (FILL_SUB_COOL_ON_BIT_OFFSET, FILL_SUB_COOL_ON_BYTE_OFFSET, true);
      }
      else
      { // V2
        // should not be here, V19 not defined in V2 protocol
        THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"),
                      _CPTC("V19 not a valid valve name in V2 protocol!"),
                      _CPTC("DigitalValve::Open")); 	    
      }
    }
    else
    {
      // Manage V2 protocol
      if (!is_protocol_V2)
      { // V1
        SetBool (this->man_off_bit_offset, this->man_off_byte_offset, true);
        SetBool (this->man_on_bit_offset, this->man_on_byte_offset, true);
      }
      else
      { // V2
        SendCmd(man_on_byte_offset);
      }
    }
  }

  //- Close ------------------------------
  void DigitalValve::Close ()
  {
    DEBUG_STREAM  << " DigitalValve::Close Digital Valve " 
                  << this->name 
                  << " ValvManOff address " 
                  << this->man_off_byte_offset
                  << "."
                  << this->man_off_bit_offset
                  << std::endl;

     if (this->setup_error)
      THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"),
                      _CPTC("invalid valve name -- Legal Names are [V9|V15|V17.1|V17.2|V19|V20|V21|VentPipe]"),
                      _CPTC("DigitalValve::Close")); 

    //- special case : SubCooler filling
    if (this->name == "V19")
    {
      // Manage V2 protocol
      if (!is_protocol_V2)
      { // V1
        SetBool (FILL_SUB_COOL_ON_BIT_OFFSET, FILL_SUB_COOL_ON_BYTE_OFFSET, false);
      }
	  else
	  { // V2
	    // should not be here, V19 not defined in V2 protocol
        THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"),
                      _CPTC("V19 not a valid valve name in V2 protocol!"),
                      _CPTC("DigitalValve::Close")); 	    
	  } 
	}
    else
    {
      // Manage V2 protocol
      if (!is_protocol_V2)
      { // V1
        SetBool (this->man_on_bit_offset, this->man_on_byte_offset, false);
        SetBool (this->man_off_bit_offset, this->man_off_byte_offset, false);
      }
      else
      { // V2
        SendCmd(man_off_byte_offset);
      }
    }
  }

  //- Status ------------------------------
  short DigitalValve::State (void)
  {
    if (this->setup_error)
      return this->state = -1;

    if (get_com_state () != HWProxy_ns::HWP_NO_ERROR)
      return get_com_state ();

    this->state = GetInt (this->status_byte_offset);
    //- update the private members is_opened and is_closed

    if (this->state == 0)
    {
      this->is_closed = true;
      this->is_opened = false;
    }
    else if (state == 1) 
    {
      this->is_closed = false;
      this->is_opened = true;
    }
    else
    {
      this->is_closed = false;
      this->is_opened = false;
    }

    return this->state;
  }

  //- Status ------------------------------
  std::string DigitalValve::Status (void)
  {
    //- setup error 
    if (this->setup_error)
      return status;

    //- communication error
    status = HWProxy_ns::hw_state_str [get_com_state ()];

    if (get_com_state () != HWProxy_ns::HWP_NO_ERROR)
      status += get_com_status ();
    else
      status += "\ndevice is up and running";

    short tmp = GetInt (this->status_byte_offset);
    if(tmp == 0)
      status += "\nValve Closed";
    else if (tmp == 1)
      status += "\nValve Open";
    else if (tmp == 2)
      status += "\nValve Fault";
    else
      status += "\nUnknown Valve Status";
    return this->status;
  }


  //---------------------------------------------------------------------
  //- Analog Class Implementation----------------------------------------
  //---------------------------------------------------------------------
  AnalogValve::AnalogValve (Tango::DeviceImpl * _host_device,
                            std::string _name) : 
                                  Part (_host_device),
                                  name (_name), 
                                  setup_error (false)
  {
    DEBUG_STREAM << "AnalogValve::AnalogValve buiding "
                 << this->name 
                 << std::endl;


    //-Legal Values are [V10|V11|V17|V19A]";
    if(name == "V10")
    {
      man_setpoint_offset = MV_MAN_V10_OFFSET;
      status_offset       = Q_OP_STATE_V10_OFFSET;
      scaled_offset       = Q_MV_SCALE_V10_OFFSET;

      // Manage V2 protocol =====>
      if (is_protocol_V2)
      {
        man_setpoint_offset = SET_V10;
      }
      // <=====
    }
    else if(name == "V11")
    {
      man_setpoint_offset = MV_MAN_V11_OFFSET;
      status_offset       = Q_OP_STATE_V11_OFFSET;
      scaled_offset       = Q_MV_SCALE_V11_OFFSET;

      // Manage V2 protocol =====>
      if (is_protocol_V2)
      {
        man_setpoint_offset = SET_V11;
      }
      // <=====
    }
    else if(name == "V17")
    {
      man_setpoint_offset = MV_MAN_V17_OFFSET;
      status_offset       = Q_OP_STATE_V17_OFFSET;
      scaled_offset       = Q_MV_SCALE_V17_OFFSET;

      // Manage V2 protocol =====>
      if (is_protocol_V2)
      { // V2
        this->setup_error = true;
        status = name + " Name Error\n -- In V2 protocol, V17 valve is not defined!" ;
        state = -1;
      }
      // <=====
	}
    else if(name == "V19A")
    {
      man_setpoint_offset = SET_V19;
      status_offset       = Q_OP_STATE_V19_OFFSET_V2;
      scaled_offset       = Q_MV_SCALE_V19_OFFSET_V2;
	  
      // Manage V2 protocol =====>
      if (!is_protocol_V2)
      { // not defined in V1 protocol
        this->setup_error = true;
        status = name + " Name Error\n -- In V1 protocol, V19 is a digital valve (see V19)!" ;
        state = -1;
      }
      // <=====	  
    }
    else
    {
      //- 
      this->setup_error = true;
      status = name + " Name Error\n -- Legal Names are[V10|V11|V17|V19A]" ;
      state = -1;
    }
  }

  //- Dtor ------------------------------
  AnalogValve::~AnalogValve (void)
  {
    //- noop DTor
    DEBUG_STREAM << " AnalogValve::~AnalogValve deleting "
                 << this->name 
                 << std::endl;
  }

  //- Open ------------------------------
  void AnalogValve::Open (float val)
  {
    DEBUG_STREAM << " AnalogValve::Open Analog Valve " 
      << this->name 
      << " MV_MAN_VXX address " 
      << this->man_setpoint_offset
      << " value = "
      << val
      << std::endl;

    if (this->setup_error)
      THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"),
                      _CPTC("invalid valve name -- Legal Names are [V10|V11|V17]"),
                      _CPTC("AnalogValve::Open")); 

    if (val < 0. || val > 100.)
      THROW_DEVFAILED(_CPTC("DATA_OUT_OF_RANGE"),
                      _CPTC("value out of range - should be in (%) [0.0 ... 100.0]"),
                      _CPTC("AnalogValve::Open"));

    SetReal (this->man_setpoint_offset, val);
  }

  //- Close ------------------------------
  void AnalogValve::Close (void)
  {
    DEBUG_STREAM << " AnalogValve::Close Analog Valve " 
      << this->name 
      << " MV_MAN_VXX address " 
      << this->man_setpoint_offset
      << std::endl;

    if (this->setup_error)
      THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"),
                      _CPTC("invalid valve name -- Legal Names are [V10|V11|V17]"),
                      _CPTC("AnalogValve::Close")); 

    this->Open (0.);
  }

  //- Get state ------------
  short AnalogValve::State (void)
  {
    //DEBUG_STREAM << " AnalogValve::State <- " << std::endl;
    if (this->setup_error)
    {
      this->state = -1;
      return this->state;
    }

    if (get_com_state () != HWProxy_ns::HWP_NO_ERROR)
      return get_com_state ();

    this->state = GetInt(this->status_offset);

    return this->state;
  }

  //- Status ------------------------------
  std::string AnalogValve::Status (void)
  {
    //DEBUG_STREAM << " AnalogValve::Status <- " << std::endl;
    //- setup error 
    if (this->setup_error)
      return status;

    //- communication error
    status = HWProxy_ns::hw_state_str [get_com_state ()];

    if (get_com_state () != HWProxy_ns::HWP_NO_ERROR)
      status += get_com_status ();
    else
      status += "\ndevice is up and running";

    short tmp = GetInt (this->status_offset);

    if(tmp == 0)
      status += "\nValve Closed";
    else if (tmp == 1)
      status += "\nValve Open";
    else if (tmp == 2)
      status += "\nValve Fault";
    else
      status += "\nUnknown Valve Status";

    return this->status;
  }

  //- Get scaled aperture (%) ----------
  float AnalogValve::Scaled (void)
  {
    //DEBUG_STREAM << " AnalogValve::Scaled Get Scaled Analog Valve " << std::endl;

    if (this->setup_error)
      THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"),
                      _CPTC("invalid valve name -- Legal Names are [V10|V11|V17]"),
                      _CPTC("AnalogValve::Scaled")); 

    this->scaled = GetReal(this->scaled_offset);
    return this->scaled;
  }


  //---------------------------------------------------------------------
  //- Temperature Implementation ----------------------------------------
  //---------------------------------------------------------------------
  Temperature::Temperature   (Tango::DeviceImpl * _host_device,
                              std::string _name) : 
                                    Part (_host_device),
                                    name (_name),
                                    setup_error (false)
  {
    DEBUG_STREAM << "Temperature::Temperature buiding "
                 << this->name 
                 << std::endl;
    //- Legal names are [T5|T6]
    status = "initializing...";

    if (name == "T5")
    {
      value_offset                   = PV_T5_OFFSET;
      low_limit_offset               = LL1_EXT_T5_OFFSET;
      high_limit_offset              = HL1_EXT_T5_OFFSET;
      high_limit_tripped_byte_offset = T5_H_LIM_VALUE_TRIPPED_BYTE_OFFSET;
      high_limit_tripped_bit_offset  = T5_H_LIM_VALUE_TRIPPED_BIT_OFFSET;
      low_limit_tripped_byte_offset  = T5_L_LIM_VALUE_TRIPPED_BYTE_OFFSET;
      low_limit_tripped_bit_offset   = T5_L_LIM_VALUE_TRIPPED_BIT_OFFSET;

      // Manage V2 protocol =====>
      low_limit_cmd = 0;
      high_limit_cmd = 0;

      if (is_protocol_V2)
      {
        low_limit_cmd = SET_T5_LL1;
        high_limit_cmd = SET_T5_HL1;
      }
      // <=====
    }
    else if (name == "T6")
    {
      value_offset                   = PV_T6_OFFSET;
      low_limit_offset               = LL1_EXT_T6_OFFSET;
      high_limit_offset              = HL1_EXT_T6_OFFSET;
      high_limit_tripped_byte_offset = T6_H_LIM_VALUE_TRIPPED_BYTE_OFFSET;
      high_limit_tripped_bit_offset  = T6_H_LIM_VALUE_TRIPPED_BIT_OFFSET;
      low_limit_tripped_byte_offset  = T6_L_LIM_VALUE_TRIPPED_BYTE_OFFSET;
      low_limit_tripped_bit_offset   = T6_L_LIM_VALUE_TRIPPED_BIT_OFFSET;

      // Manage V2 protocol =====>
      low_limit_cmd = 0;
      high_limit_cmd = 0;

      if (is_protocol_V2)
      {
        low_limit_cmd = SET_T6_LL1;
        high_limit_cmd = SET_T6_HL1;
      }
      // <=====
    }
    else
    {
      this->setup_error = true;
      status = name + " Name Error\n -- Legal Names are[T5|T6]";
      return;
    }

    //- get low and high limits
    try
    {
      Low_limit ();
      High_limit ();
    }
    catch (Tango::DevFailed &e)
    {
      ERROR_STREAM << " Temperature::Temperature for " 
        << name 
        << " catched Devfailed ["
        << e
        << "[ trying to get low and high limits"
        << endl;
    }
    catch (...)
    {
      ERROR_STREAM << "  Temperature::Temperature  for " 
        << name 
        << " catched (...) trying to get low and high limits"
        << endl;
    }
  }

  //- Dtor ------------------------------
  Temperature::~Temperature (void)
  {
    //- noop DTor
    DEBUG_STREAM << "Temperature::~Temperature deleting "
                 << this->name 
                 << std::endl;
  }

  //- Get value -----------------------
  float Temperature::Value (void)
  {
    //DEBUG_STREAM << "Temperature::value Get value " << std::endl;

    if (this->setup_error)
      THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"),
                    _CPTC("invalid Temperature Probe name -- Legal Names are [T5|T6]"),
                    _CPTC("Temperature::Value")); 

    this->value = GetReal(this->value_offset);
    return this->value;
  }

  //- Get low limit -------------------
  float Temperature::Low_limit (void)
  {
    //DEBUG_STREAM << "Temperature::low_limit Get value "  << std::endl;

    if (this->setup_error)
      THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"),
                    _CPTC("invalid Temperature Probe name -- Legal Names are [T5|T6]"),
                    _CPTC("Temperature::Low_limit")); 

    this->low_limit = GetReal(this->low_limit_offset);
    return this->low_limit;
  }

  //- Set low limit ------------------------
  void Temperature::Low_limit (float val)
  {
    if (this->setup_error)
      THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"),
                    _CPTC("invalid Temperature Probe name -- Legal Names are [T5|T6]"),
                    _CPTC("Temperature::Low_limit"));

    if (val < 73. || val > 320.)
      THROW_DEVFAILED(_CPTC("DATA_OUT_OF_RANGE"),
                      _CPTC("value out of range - should be in (K) [73.0 ... 320.0]"),
                      _CPTC("Temperature::Low_limit")); 

    //- Low Limit should be < High Limit
    if ( val >= this->high_limit)
      THROW_DEVFAILED(_CPTC("DATA_OUT_OF_RANGE"),
                      _CPTC("value out of range - low limit should be < high limit"),
                      _CPTC("Temperature::Low_limit")); 
    
    // Manage V2 protocol
    if (!is_protocol_V2)
    { // V1
      DEBUG_STREAM << " Temperature::low_limit writing low_limit " 
        << this->name 
        << " LL1_EXT_Tx address " 
        << this->low_limit_offset
        << std::endl;

      SetReal (this->low_limit_offset, val);
    }
    else
    { // V2
      DEBUG_STREAM << " Temperature::low_limit writing low_limit " 
        << this->name 
        << " LL1_EXT_Tx address " 
        << this->low_limit_cmd
        << std::endl;

      SetReal (this->low_limit_cmd, val);
    }

    this->low_limit = val;
}

  //- Get high limit ------------------
  float Temperature::High_limit (void)
  {
    //DEBUG_STREAM << "Temperature::High_limit Get value " << std::endl;

    if (this->setup_error)
      THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"),
                    _CPTC("invalid Temperature Probe name -- Legal Names are [T5|T6]"),
                    _CPTC("Temperature::High_limit"));
 
    this->high_limit = GetReal(this->high_limit_offset);
    return this->high_limit;
  }

  //- Set High limit ------------------------
  void Temperature::High_limit (float val)
  {
    if (this->setup_error)
      THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"),
                    _CPTC("invalid Temperature Probe name -- Legal Names are [T5|T6]"),
                    _CPTC("Temperature::High_limit"));

    if (val < 73. || val > 320.)
      THROW_DEVFAILED(_CPTC("DATA_OUT_OF_RANGE"),
                      _CPTC("value out of range - should be in (K) [73.0 ... 320.0]"),
                      _CPTC("Temperature::High_limit")); 

    //- High Limit should be > Low Limit
    if ( val <= this->low_limit)
      THROW_DEVFAILED(_CPTC("DATA_OUT_OF_RANGE"),
                      _CPTC("value out of range - high limit should be > low limit"),
                      _CPTC("Temperature::High_limit")); 

    // Manage V2 protocol
    if (!is_protocol_V2)
    { // V1
      DEBUG_STREAM << " Temperature::High_limit writing High_limit "
        << this->name 
        << " HL1_EXT_Tx address " 
        << this->high_limit_offset
        << std::endl;

      SetReal (this->high_limit_offset, val);
    }
    else
    { // V2
      DEBUG_STREAM << " Temperature::High_limit writing High_limit "
        << this->name 
        << " HL1_EXT_Tx address " 
        << this->high_limit_cmd
        << std::endl;

      SetReal (this->high_limit_cmd, val);
    }

    this->high_limit = val;
  }

  //- Get low limit ------------------------
  bool Temperature::LowLimitTripped (void)
  { 
    //DEBUG_STREAM << " Temperature::LowLimitTripped entering " << std::endl;

    if (this->setup_error)
      THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"),
                    _CPTC("invalid Temperature Probe name -- Legal Names are [T5|T6]"),
                    _CPTC("Temperature::LowLimitTripped"));

   return GetBool (low_limit_tripped_bit_offset, low_limit_tripped_byte_offset); 
  }

  //- Get high limit ------------------------
  bool Temperature::HighLimitTripped (void)
  { 
    //DEBUG_STREAM << " Temperature::HighLimitTripped entering " << std::endl;

    if (this->setup_error)
      THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"),
                    _CPTC("invalid Temperature Probe name -- Legal Names are [T5|T6]"),
                    _CPTC("Temperature::HighLimitTripped"));

    return GetBool (high_limit_tripped_bit_offset, high_limit_tripped_byte_offset); 
  }

  //- Get state ------------
  short Temperature::State (void)
  {
    //DEBUG_STREAM << " Temperature::state <- " << std::endl;

    if (this->setup_error)
        return -1;

    return get_com_state ();
  }

  //- Status ------------------------------
  std::string Temperature::Status (void)
  {
    //DEBUG_STREAM << " Temperature::status <- " << std::endl;

    //- setup error 
    if (this->setup_error)
      return status;

    //- communication error
    status = HWProxy_ns::hw_state_str [get_com_state ()];

    if (get_com_state () != HWProxy_ns::HWP_NO_ERROR)
      status += get_com_status ();
    else
      status += "\ndevice is up and running";

    return this->status;
  }


  //---------------------------------------------------------------------
  //- Level Implementation ----------------------------------------------
  //---------------------------------------------------------------------
  Level::Level   (Tango::DeviceImpl * _host_device, 
                  std::string _name) : 
                    Part (_host_device),
                    name (_name),
                    setup_error (false)
  {
    DEBUG_STREAM << "Level::Level building "
                 << this->name 
                 << std::endl;
    status = "initializing...";

    if (name == "LT19")
    {
      value_offset                   = PV_LT19_OFFSET;
      low_limit_offset               = LL1_EXT_LT19_OFFSET;
      high_limit_offset              = HL1_EXT_LT19_OFFSET;
      high_limit_tripped_byte_offset = LT19_H_LIM_VALUE_TRIPPED_BYTE_OFFSET;
      high_limit_tripped_bit_offset  = LT19_H_LIM_VALUE_TRIPPED_BIT_OFFSET;
      low_limit_tripped_byte_offset  = LT19_L_LIM_VALUE_TRIPPED_BYTE_OFFSET;
      low_limit_tripped_bit_offset   = LT19_L_LIM_VALUE_TRIPPED_BIT_OFFSET;
      high_alarm_tripped_byte_offset = LT19_H_ALM_VALUE_TRIPPED_BYTE_OFFSET;
      high_alarm_tripped_bit_offset  = LT19_H_ALM_VALUE_TRIPPED_BIT_OFFSET;
      low_alarm_tripped_byte_offset  = LT19_L_ALM_VALUE_TRIPPED_BYTE_OFFSET;
      low_alarm_tripped_bit_offset   = LT19_L_ALM_VALUE_TRIPPED_BIT_OFFSET;

      // Manage V2 protocol =====>
      low_limit_cmd = 0;
      high_limit_cmd = 0;
      manual_mode_cmd = 0;
      decay_mode_cmd = 0;
      topup_mode_cmd = 0;
      manual_mode_on_byte_offset = 0;
      manual_mode_on_bit_offset = 0;
      decay_mode_on_byte_offset = 0;
      decay_mode_on_bit_offset = 0;
      topup_mode_on_byte_offset = 0;
      topup_mode_on_bit_offset = 0;

      if (is_protocol_V2)
      {
        low_limit_cmd = SET_LT19_LL1;
        high_limit_cmd = SET_LT19_HL1;
        manual_mode_cmd = SET_LT19_MAN_MODE;
        decay_mode_cmd = SET_LT19_DECAY_MODE;
        topup_mode_cmd = SET_LT19_TOPUP_MODE;
        manual_mode_on_byte_offset = LT19_MAN_MODE_ON_BYTE_OFFSET;
        manual_mode_on_bit_offset = LT19_MAN_MODE_ON_BIT_OFFSET;
        decay_mode_on_byte_offset = LT19_DECAY_MODE_ON_BYTE_OFFSET;
        decay_mode_on_bit_offset = LT19_DECAY_MODE_ON_BIT_OFFSET;
        topup_mode_on_byte_offset = LT19_TOPUP_MODE_ON_BYTE_OFFSET;
        topup_mode_on_bit_offset = LT19_TOPUP_MODE_ON_BIT_OFFSET;
      }
      // <=====
    }
    else if (name == "LT23")
    {
      value_offset                   = PV_LT23_OFFSET;
      low_limit_offset               = LL1_EXT_LT23_OFFSET;
      high_limit_offset              = HL1_EXT_LT23_OFFSET;
      high_limit_tripped_byte_offset = LT23_H_LIM_VALUE_TRIPPED_BYTE_OFFSET;
      high_limit_tripped_bit_offset  = LT23_H_LIM_VALUE_TRIPPED_BIT_OFFSET;
      low_limit_tripped_byte_offset  = LT23_L_LIM_VALUE_TRIPPED_BYTE_OFFSET;
      low_limit_tripped_bit_offset   = LT23_L_LIM_VALUE_TRIPPED_BIT_OFFSET;
      high_alarm_tripped_byte_offset = LT23_H_ALM_VALUE_TRIPPED_BYTE_OFFSET;
      high_alarm_tripped_bit_offset  = LT23_H_ALM_VALUE_TRIPPED_BIT_OFFSET;
      low_alarm_tripped_byte_offset  = LT23_L_ALM_VALUE_TRIPPED_BYTE_OFFSET;
      low_alarm_tripped_bit_offset   = LT23_L_ALM_VALUE_TRIPPED_BIT_OFFSET;

      // Manage V2 protocol =====>
      low_limit_cmd = 0;
      high_limit_cmd = 0;
      manual_mode_cmd = 0;
      decay_mode_cmd = 0;
      topup_mode_cmd = 0;
      manual_mode_on_byte_offset = 0;
      manual_mode_on_bit_offset = 0;
      decay_mode_on_byte_offset = 0;
      decay_mode_on_bit_offset = 0;
      topup_mode_on_byte_offset = 0;
      topup_mode_on_bit_offset = 0;

      if (is_protocol_V2)
      {
        low_limit_cmd = SET_LT23_LL1;
        high_limit_cmd = SET_LT23_HL1;
      }
      // <=====
    }
    else
    {
      setup_error = true;
      status = name + " Name Error\n -- Legal Names are[LT19|LT23]";
      return;
    }

    //- get low and high limit at init
    try
    {
      Low_limit ();
      High_limit ();
    }
    catch (Tango::DevFailed &e)
    {
      ERROR_STREAM << " Level::Level for " 
        << name 
        << " catched Devfailed ["
        << e
        << "[ trying to get low and high limits"
        << endl;
    }
    catch (...)
    {
      ERROR_STREAM << "  Level::Level  for " 
        << name 
        << " catched (...) trying to get low and high limits"
        << endl;
    }
  }

  //- Dtor ------------------------------
  Level::~Level (void)
  {
    //- noop DTor
    DEBUG_STREAM << "Level::~Level deleting "
                 << this->name 
                 << std::endl;
  }

  //- Get value -----------------------
  float Level::Value (void)
  {
    //DEBUG_STREAM << "Level::value Get value " << std::endl;

    if (this->setup_error)
      THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"),
                    _CPTC("invalid Level Probe name -- Legal Names are [LT19|LT23]"),
                    _CPTC("Level::Value")); 

    this->value = GetReal(this->value_offset);
    return this->value;
  }

  //- Get low limit -------------------
  float Level::Low_limit (void)
  {
    //DEBUG_STREAM << "Level::low_limit Get value "  << std::endl;

    if (this->setup_error)
      THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"),
                    _CPTC("invalid Level Probe name -- Legal Names are [LT19|LT23]"),
                    _CPTC("Level::Low_limit")); 

    this->low_limit = GetReal(this->low_limit_offset);
    return this->low_limit;
  }

  //- Set low limit ------------------------
  void Level::Low_limit (float val)
  {
    if (this->setup_error)
      THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"),
                    _CPTC("invalid Level Probe name -- Legal Names are [LT19|LT23]"),
                    _CPTC("Level::Low_limit")); 

    if (val < 0. || val > 100.)
      THROW_DEVFAILED(_CPTC("DATA_OUT_OF_RANGE"),
                      _CPTC("value out of range - should be in (%) [0.0 ... 100.0]"),
                      _CPTC("Level::Low_limit")); 

    //- Low Limit should be < High Limit
    if ( val >= this->high_limit)
      THROW_DEVFAILED(_CPTC("DATA_OUT_OF_RANGE"),
                      _CPTC("value out of range - low limit should be < high limit"),
                      _CPTC("Level::Low_limit")); 

    // Manage V2 protocol
    if (!is_protocol_V2)
    { // V1
      DEBUG_STREAM << " Level::low_limit writing low_limit " 
        << this->name 
        << " LL1_EXT_Tx address " 
        << this->low_limit_offset
        << std::endl;

      SetReal (this->low_limit_offset, val);
    }
    else
    { // V2
      DEBUG_STREAM << " Level::low_limit writing low_limit " 
        << this->name 
        << " LL1_EXT_Tx address " 
        << this->low_limit_cmd
        << std::endl;

      SetReal (this->low_limit_cmd, val);
    }
  }

  //- Get high limit ------------------
  float Level::High_limit (void)
  {
    //DEBUG_STREAM << "Level::value Get value " << std::endl;

    if (this->setup_error)
      THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"),
                      _CPTC("invalid Level Probe name -- Legal Names are [LT19|LT23]"),
                      _CPTC("Level::High_limit")); 

    this->high_limit = GetReal(this->high_limit_offset);
    return this->high_limit;
  }

  //- Set High limit ------------------------
  void Level::High_limit (float val)
  {
    if (this->setup_error)
      THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"),
                    _CPTC("invalid Level Probe name -- Legal Names are [LT19|LT23]"),
                    _CPTC("Level::High_limit")); 

    if (val < 0. || val > 100.)
      THROW_DEVFAILED(_CPTC("DATA_OUT_OF_RANGE"),
                      _CPTC("value out of range - should be in (%) [0.0 ... 100.0]"),
                      _CPTC("Level::High_limit")); 

    //- High Limit should be > Low Limit
    if ( val <= this->low_limit)
      THROW_DEVFAILED(_CPTC("DATA_OUT_OF_RANGE"),
                      _CPTC("value out of range - high limit should be > low limit"),
                      _CPTC("Level::High_limit")); 

    // Manage V2 protocol
    if (!is_protocol_V2)
    { // V1
      DEBUG_STREAM << " Level::High_limit writing High_limit " 
        << this->name 
        << " HL1_EXT_Tx address " 
        << this->high_limit_offset
        << std::endl;

      SetReal (this->high_limit_offset, val);
    }
    else
    { // V2
      DEBUG_STREAM << " Level::High_limit writing High_limit " 
        << this->name 
        << " HL1_EXT_Tx address " 
        << this->high_limit_cmd
        << std::endl;

      SetReal (this->high_limit_cmd, val);
    }
  }

  //- Get low limit ------------------------
  bool Level::LowLimitTripped (void)
  { 
    //DEBUG_STREAM << "Level::LowLimitTripped entering " << std::endl;

    if (this->setup_error)
      THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"),
                    _CPTC("invalid Level Probe name -- Legal Names are [LT19|LT23]"),
                    _CPTC("Level::LowLimitTripped")); 

    return GetBool (low_limit_tripped_bit_offset, low_limit_tripped_byte_offset); 
  }

  //- Get high limit ------------------------
  bool Level::HighLimitTripped (void)
  { 
    //DEBUG_STREAM << "Level::HighLimitTripped entering " << std::endl;

    if (this->setup_error)
      THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"),
                    _CPTC("invalid Level Probe name -- Legal Names are [LT19|LT23]"),
                    _CPTC("Level::HighLimitTripped")); 

    return GetBool (high_limit_tripped_bit_offset, high_limit_tripped_byte_offset); 
  }

  //- Get low alarm ------------------------
  bool Level::LowAlarmTripped (void)
  { 
    //DEBUG_STREAM << "Level::LowAlarmtTripped entering " << std::endl;

    if (this->setup_error)
      THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"),
                    _CPTC("invalid Level Probe name -- Legal Names are [LT19|LT23]"),
                    _CPTC("Level::LowAlarmTripped")); 

    return GetBool (low_alarm_tripped_bit_offset, low_alarm_tripped_byte_offset); 
  }

  //- Get high alarm ------------------------
  bool Level::HighAlarmTripped (void)
  { 
    //DEBUG_STREAM << "Level::HighAlarmTripped entering " << std::endl;

    if (this->setup_error)
      THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"),
                    _CPTC("invalid Level Probe name -- Legal Names are [LT19|LT23]"),
                    _CPTC("Level::HighAlarmTripped")); 

    return GetBool (high_alarm_tripped_bit_offset, high_alarm_tripped_byte_offset); 
  }

  //- Get Manual Mode On ------------------------
  bool Level::ManModeOn (void)
  { 
    //DEBUG_STREAM << "Level::ManModeOn entering " << std::endl;

    if (this->setup_error)
      THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"),
                    _CPTC("invalid Level Probe name -- Legal Names are [LT19|LT23]"),
                    _CPTC("Level::ManModeOn")); 

    if (!is_protocol_V2)
      THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"),
                      _CPTC("Mode state only defined for V2 protocol!"),
                      _CPTC("Level::ManModeOn")); 

    if (name != "LT19")
      THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"),
                      _CPTC("Mode state only defined for LT19!"),
                      _CPTC("Level::ManModeOn"));

    return GetBool (manual_mode_on_bit_offset, manual_mode_on_byte_offset); 
  }

  //- Get Decay Mode On ------------------------
  bool Level::DecayModeOn (void)
  { 
    //DEBUG_STREAM << "Level::DecayModeOn entering " << std::endl;

    if (this->setup_error)
      THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"),
                    _CPTC("invalid Level Probe name -- Legal Names are [LT19|LT23]"),
                    _CPTC("Level::DecayModeOn")); 

    if (!is_protocol_V2)
      THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"),
                      _CPTC("Mode state only defined for V2 protocol!"),
                      _CPTC("Level::DecayModeOn")); 

    if (name != "LT19")
      THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"),
                      _CPTC("Mode state only defined for LT19!"),
                      _CPTC("Level::DecayModeOn"));

    return GetBool (decay_mode_on_bit_offset, decay_mode_on_byte_offset); 
  }

  //- Get Topup Mode On ------------------------
  bool Level::TopupModeOn (void)
  { 
    //DEBUG_STREAM << "Level::TopupModeOn entering " << std::endl;

    if (this->setup_error)
      THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"),
                    _CPTC("invalid Level Probe name -- Legal Names are [LT19|LT23]"),
                    _CPTC("Level::TopupModeOn")); 

    if (!is_protocol_V2)
      THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"),
                      _CPTC("Mode state only defined for V2 protocol!"),
                      _CPTC("Level::TopupModeOn")); 

    if (name != "LT19")
      THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"),
                      _CPTC("Mode state only defined for LT19!"),
                      _CPTC("Level::TopupModeOn"));

    return GetBool (topup_mode_on_bit_offset, topup_mode_on_byte_offset); 
  }

  //- Set Manual Mode On ------------------------------
  void Level::SetManMode ()
  {
    if (this->setup_error)
      THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"),
                    _CPTC("invalid Level Probe name -- Legal Names are [LT19|LT23]"),
                    _CPTC("Level::SetManMode")); 

    if (!is_protocol_V2)
      THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"),
                      _CPTC("Mode command only defined for V2 protocol!"),
                      _CPTC("Level::SetManMode")); 

    if (name != "LT19")
      THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"),
                      _CPTC("Mode command only defined for LT19!"),
                      _CPTC("Level::SetManMode"));

    DEBUG_STREAM  << " Level::SetManMode Level " 
                  << this->name 
                  << " SetManMode address " 
                  << this->manual_mode_cmd
                  << std::endl;

    SendCmd(manual_mode_cmd);
  }

  //- Set Decay Mode On ------------------------------
  void Level::SetDecayMode ()
  {
    if (this->setup_error)
      THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"),
                    _CPTC("invalid Level Probe name -- Legal Names are [LT19|LT23]"),
                    _CPTC("Level::SetDecayMode")); 

    if (!is_protocol_V2)
      THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"),
                      _CPTC("Mode command only defined for V2 protocol!"),
                      _CPTC("Level::SetDecayMode")); 

    if (name != "LT19")
      THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"),
                      _CPTC("Mode command only defined for LT19!"),
                      _CPTC("Level::SetDecayMode"));

    DEBUG_STREAM  << " Level::SetDecayMode Level " 
                  << this->name 
                  << " SetDecayMode address " 
                  << this->decay_mode_cmd
                  << std::endl;

    SendCmd(decay_mode_cmd);
  }

  //- Set Topup Mode On ------------------------------
  void Level::SetTopupMode ()
  {
    if (this->setup_error)
      THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"),
                    _CPTC("invalid Level Probe name -- Legal Names are [LT19|LT23]"),
                    _CPTC("Level::SetTopupMode")); 

    if (!is_protocol_V2)
      THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"),
                      _CPTC("Mode command only defined for V2 protocol!"),
                      _CPTC("Level::SetTopupMode")); 

    if (name != "LT19")
      THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"),
                      _CPTC("Mode command only defined for LT19!"),
                      _CPTC("Level::SetTopupMode"));

    DEBUG_STREAM  << " Level::SetTopupMode Level " 
                  << this->name 
                  << " SetTopupMode address " 
                  << this->topup_mode_cmd
                  << std::endl;

    SendCmd(topup_mode_cmd);
  }

  //- Get state ------------
  short Level::State (void)
  {
    //DEBUG_STREAM << " Level::state <- " << std::endl;

    if (this->setup_error)
        return -1;

    return get_com_state ();
  }

  //- Status ------------------------------
  std::string Level::Status (void)
  {
    //DEBUG_STREAM << " Level::status <- " << std::endl;

    //- setup error 
    if (this->setup_error)
      return status;

    //- communication error
    status = HWProxy_ns::hw_state_str [get_com_state ()];

    if (get_com_state () != HWProxy_ns::HWP_NO_ERROR)
      status += get_com_status ();
    else
      status += "\ndevice is up and running";

    return this->status;
  }


  //---------------------------------------------------------------------
  //- Pressure Implementation
  //---------------------------------------------------------------------

  //- CTor ----------------------------------
  Pressure::Pressure   (Tango::DeviceImpl * _host_device,
                        std::string _name) : 
                        Part (_host_device),
                        name (_name),
                        setup_error (false)
  {
    INFO_STREAM << "Pressure::Pressure buiding "
                 << this->name 
                 << std::endl;
    status = "innitializing...";

    if (this->name == "PT1")
    {
      value_offset                   = PV_PT1_OFFSET;
      low_limit_offset               = LL1_EXT_PT1_OFFSET;
      high_limit_offset              = HL1_EXT_PT1_OFFSET;
      high_limit_tripped_byte_offset = PT1_H_LIM_VALUE_TRIPPED_BYTE_OFFSET;
      high_limit_tripped_bit_offset  = PT1_H_LIM_VALUE_TRIPPED_BIT_OFFSET;
      low_limit_tripped_byte_offset  = PT1_L_LIM_VALUE_TRIPPED_BYTE_OFFSET;
      low_limit_tripped_bit_offset   = PT1_L_LIM_VALUE_TRIPPED_BIT_OFFSET;

      // Manage V2 protocol =====>
      low_limit_cmd = 0;
      high_limit_cmd = 0;

      if (is_protocol_V2)
      {
        low_limit_cmd = SET_PT1_LL1;
        high_limit_cmd = SET_PT1_HL1;
      }
      // <=====
    }
    else if (this->name == "PT3")
    {
      value_offset                   = PV_PT3_OFFSET;
      low_limit_offset               = LL1_EXT_PT3_OFFSET;
      high_limit_offset              = HL1_EXT_PT3_OFFSET;
      high_limit_tripped_byte_offset = PT3_H_LIM_VALUE_TRIPPED_BYTE_OFFSET;
      high_limit_tripped_bit_offset  = PT3_H_LIM_VALUE_TRIPPED_BIT_OFFSET;
      low_limit_tripped_byte_offset  = PT3_L_LIM_VALUE_TRIPPED_BYTE_OFFSET;
      low_limit_tripped_bit_offset   = PT3_L_LIM_VALUE_TRIPPED_BIT_OFFSET;

      // Manage V2 protocol =====>
      low_limit_cmd = 0;
      high_limit_cmd = 0;

      if (is_protocol_V2)
      {
        low_limit_cmd = SET_PT3_LL1;
        high_limit_cmd = SET_PT3_HL1;
      }
      // <=====
    }
    else
    {
      this->setup_error = true;
      status = name + " Name Error\n -- Legal Names are[PT1|PT3]";
      return;
    }

    //- read back low & high limits
    try
    {
      Low_limit ();
      High_limit ();
    }
    catch (Tango::DevFailed &e)
    {
      ERROR_STREAM << " Pressure::Pressure for " 
        << name 
        << " catched Devfailed ["
        << e
        << "[ trying to get low and high limits"
        << endl;
    }
    catch (...)
    {
      ERROR_STREAM << " Pressure::Pressure for " 
        << name 
        << " catched (...) trying to get low and high limits"
        << endl;
    }
  }

  //- Dtor ------------------------------
  Pressure::~Pressure (void)
  {
    //- noop DTor
    DEBUG_STREAM << "Pressure::~Pressure deleting "
                 << this->name 
                 << std::endl;
  }

  //- Get value -----------------------
  float Pressure::Value (void)
  {
    //DEBUG_STREAM << "Pressure::value Get value " << std::endl;

    if (this->setup_error)
      THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"),
                      _CPTC("invalid Pressure Probe name -- Legal Names are [PT1|PT3]"),
                      _CPTC("Pressure::Value")); 

    this->value = GetReal(this->value_offset);
    return this->value;
  }

  //- Get low limit -------------------
  float Pressure::Low_limit (void)
  {
    //DEBUG_STREAM << "Pressure::low_limit Get value "  << std::endl;

    if (this->setup_error)
      THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"),
                      _CPTC("invalid Pressure Probe name -- Legal Names are [PT1|PT3]"),
                      _CPTC("Pressure::Low_limit")); 

    this->low_limit = GetReal(this->low_limit_offset);
    return this->low_limit;
  }

  //- Set low limit ------------------------
  void Pressure::Low_limit (float val)
  {
    if (this->setup_error)
      THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"),
                      _CPTC("invalid Pressure Probe name -- Legal Names are [PT1|PT3]"),
                      _CPTC("Pressure::Low_limit")); 

    if (val < 0. || val > 10.)
      THROW_DEVFAILED(_CPTC("DATA_OUT_OF_RANGE"),
                      _CPTC("value out of range - should be in (Bar) [0.0 ... 10.0]"),
                      _CPTC("Pressure::Low_limit")); 

    //- High Limit should be > Low Limit
    if ( val >= this->high_limit)
      THROW_DEVFAILED(_CPTC("DATA_OUT_OF_RANGE"),
                      _CPTC("value out of range - low limit should be < high limit"),
                      _CPTC("Pressure::Low_limit")); 

    // Manage V2 protocol
    if (!is_protocol_V2)
    { // V1
      DEBUG_STREAM << " Pressure::low_limit writing low_limit " 
        << this->name 
        << " LL1_EXT_Tx address " 
        << this->low_limit_offset
        << std::endl;

      SetReal (this->low_limit_offset, val);
    }
    else
    { // V2
      DEBUG_STREAM << " Pressure::low_limit writing low_limit " 
        << this->name 
        << " LL1_EXT_Tx address " 
        << this->low_limit_cmd
        << std::endl;

      SetReal (this->low_limit_cmd, val);
    }
  }

  //- Get high limit ------------------
  float Pressure::High_limit (void)
  {
    //DEBUG_STREAM << "Pressure::value Get value " << std::endl;

    if (this->setup_error)
      THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"),
                      _CPTC("invalid Pressure Probe name -- Legal Names are [PT1|PT3]"),
                      _CPTC("Pressure::High_limit")); 

    this->high_limit = GetReal(this->high_limit_offset);
    return this->high_limit;
  }

  //- Set low limit ------------------------
  void Pressure::High_limit (float val)
  {
    if (this->setup_error)
      THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"),
                      _CPTC("invalid Pressure Probe name -- Legal Names are [PT1|PT3]"),
                      _CPTC("Pressure::High_limit")); 

    if (val < 0. || val > 10.)
      THROW_DEVFAILED(_CPTC("DATA_OUT_OF_RANGE"),
                      _CPTC("value out of range - should be in (Bar) [0.0 ... 10.0]"),
                      _CPTC("Pressure::High_limit")); 

    //- High Limit should be > Low Limit
    if ( val <= this->low_limit)
      THROW_DEVFAILED(_CPTC("DATA_OUT_OF_RANGE"),
                      _CPTC("value out of range - high limit should be > low limit"),
                      _CPTC("Pressure::High_limit")); 

    // Manage V2 protocol
    if (!is_protocol_V2)
    { // V1
      DEBUG_STREAM << " Pressure::High_limit writing High_limit " 
        << this->name 
        << " HL1_EXT_Tx address " 
        << this->high_limit_offset
        << std::endl;

      SetReal (this->high_limit_offset, val);
    }
    else
    { // V2
      DEBUG_STREAM << " Pressure::High_limit writing High_limit " 
        << this->name 
        << " HL1_EXT_Tx address " 
        << this->high_limit_cmd
        << std::endl;

      SetReal (this->high_limit_cmd, val);
    }
  }

  //- Get LowLimitTripped ------------------------
  bool Pressure::LowLimitTripped (void)
  { 
    //DEBUG_STREAM << "Pressure::LowLimitTripped entering " << std::endl;

    if (this->setup_error)
      THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"),
                      _CPTC("invalid Pressure Probe name -- Legal Names are [PT1|PT3]"),
                      _CPTC("Pressure::LowLimitTripped")); 

    return GetBool (low_limit_tripped_bit_offset, low_limit_tripped_byte_offset); 
  }

  //- Get HighLimitTripped ------------------------
  bool Pressure::HighLimitTripped (void)
  { 
    //DEBUG_STREAM << "Pressure::HighLimitTripped entering " << std::endl;

    if (this->setup_error)
      THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"),
                      _CPTC("invalid Pressure Probe name -- Legal Names are [PT1|PT3]"),
                      _CPTC("Pressure::HighLimitTripped")); 

    return GetBool (high_limit_tripped_bit_offset, high_limit_tripped_byte_offset); 
  }

  //- Get state ------------
  short Pressure::State (void)
  {
    //DEBUG_STREAM << " Pressure::state <- " << std::endl;

    if (this->setup_error)
        return -1;

    return get_com_state ();
  }

  //- Status ------------------------------
  std::string Pressure::Status (void)
  {
    //DEBUG_STREAM << " Pressure::status <- " << std::endl;

    //- setup error 
    if (this->setup_error)
      return status;

    //- communication error
    status = HWProxy_ns::hw_state_str [get_com_state ()];

    if (get_com_state () != HWProxy_ns::HWP_NO_ERROR)
      status += get_com_status ();
    else
      status += "\ndevice is up and running";

    return this->status;
  }


  //---------------------------------------------------------------------
  //- Pump Class Implementation -----------------------------------------
  //---------------------------------------------------------------------
  Pump::Pump (Tango::DeviceImpl * _host_device,
              std::string _name) : 
                  Part (_host_device),
                  flow_offset (PV_FT18_OFFSET),
                  frequency_offset (PV_LN2_FREQ_OFFSET),
                  frequency_setpoint_offset (CONV_MV_MAN_OFFSET),
                  power_offset (POWER_OFFSET),
                  pump_status_offset (Q_OP_STATE_LN2_OFFSET),
                  on_bit_offset (CONV_LC_ON_BIT_OFFSET),
                  on_byte_offset (CONV_LC_ON_BYTE_OFFSET),
                  off_bit_offset (CONV_LC_OFF_BIT_OFFSET),
                  off_byte_offset (CONV_LC_OFF_BYTE_OFFSET),
                  low_limit_offset (0),
                  high_limit_offset (0),
                  high_limit_tripped_byte_offset (0),
                  high_limit_tripped_bit_offset (0),
                  low_limit_tripped_byte_offset (0),
                  low_limit_tripped_bit_offset (0),
                  low_limit_cmd (0),
                  high_limit_cmd (0),
                  high_limit(yat::IEEE_NAN),
                  low_limit(yat::IEEE_NAN),
                  name (_name)
  {
    DEBUG_STREAM << "Pump::Pump building "
                 << this->name 
                 << std::endl;

    // Manage V2 protocol =====>
    if (is_protocol_V2)
    {
      frequency_setpoint_offset = SET_LN2_PUMP_FREQ;
      on_bit_offset = 0;
      on_byte_offset = LN2_PUMP_ON;
      off_bit_offset = 0;
      off_byte_offset = LN2_PUMP_OFF;

      low_limit_offset = LL1_EXT_FT18_OFFSET;
      high_limit_offset = HL1_EXT_FT18_OFFSET;
      high_limit_tripped_byte_offset = FT18_H_LIM_VALUE_TRIPPED_BYTE_OFFSET;
      high_limit_tripped_bit_offset = FT18_H_LIM_VALUE_TRIPPED_BIT_OFFSET;
      low_limit_tripped_byte_offset = FT18_L_LIM_VALUE_TRIPPED_BYTE_OFFSET;
      low_limit_tripped_bit_offset = FT18_L_LIM_VALUE_TRIPPED_BIT_OFFSET;
      low_limit_cmd = SET_FT18_LL1;
      high_limit_cmd = SET_FT18_HL1;

      //- get low and high limits
      try
      {
        Low_limit ();
        High_limit ();
      }
      catch (Tango::DevFailed &e)
      {
        ERROR_STREAM << " Pump::Pump for " 
          << name 
          << " catched Devfailed ["
          << e
          << "[ trying to get low and high limits"
          << endl;
      }
      catch (...)
      {
        ERROR_STREAM << "  Pump::Pump  for " 
          << name 
          << " catched (...) trying to get low and high limits"
          << endl;
      }
    }
    // <======
  }

  //- Dtor ------------------------------
  Pump::~Pump (void)
  {
    //- noop DTor
    DEBUG_STREAM << " Pump::~Pump deleting "
                 << this->name 
                 << std::endl;
  }

   //- Get Flow Value ----------------------
  float Pump::Flow (void)
  {
    //DEBUG_STREAM << "Pump::Flow Get value " << std::endl;

    this->flow = GetReal(this->flow_offset);
    return this->flow;
  }

  //- Get Frequency Value ------------------
  float Pump::Frequency (void)
  {
    //DEBUG_STREAM << "Pump::Frequency Get frequency " << std::endl;

    this->frequency = GetReal(this->frequency_offset);
    return this->frequency;
  }

  //- Set frequency -----------------------
  void Pump::Frequency (float val)
  {
    DEBUG_STREAM << " Pump::Frequency Set frequency" 
      << this->name 
      << " CONV_MV_MAN address " 
      << this->frequency_setpoint_offset
      << std::endl;

    if (val < 0. || val > 80.)
      THROW_DEVFAILED(_CPTC("DATA_OUT_OF_RANGE"),
                      _CPTC("value out of range - should be in (%) [0.0 ... 80.0]"),
                      _CPTC("Pump::Frequency ")); 

    SetReal (this->frequency_setpoint_offset, val);
  }

  //- Get Power Value ---------------------
  float Pump::Power (void)
  {
    //DEBUG_STREAM << "Pump::Power Get Power value " << std::endl;

    this->power = GetReal(this->power_offset);
    return this->power;
  }

  //- Get low limit -------------------
  float Pump::Low_limit (void)
  {
    //DEBUG_STREAM << "Pump::low_limit Get value "  << std::endl;

    if (!is_protocol_V2)
      THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"),
                      _CPTC("FT18 low limit only defined for V2 protocol!"),
                      _CPTC("Pump::Low_limit")); 

    this->low_limit = GetReal(this->low_limit_offset);
    return this->low_limit;
  }

  //- Set low limit ------------------------
  void Pump::Low_limit (float val)
  {
    if (!is_protocol_V2)
      THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"),
                      _CPTC("FT18 low limit only defined for V2 protocol!"),
                      _CPTC("Pump::Low_limit")); 

    if (val < 0. || val > 20.)
      THROW_DEVFAILED(_CPTC("DATA_OUT_OF_RANGE"),
                      _CPTC("value out of range - should be in (l/min) [0.0 ... 20.0]"),
                      _CPTC("Pump::Low_limit")); 

    //- Low Limit should be < High Limit
    if ( val >= this->high_limit)
      THROW_DEVFAILED(_CPTC("DATA_OUT_OF_RANGE"),
                      _CPTC("value out of range - low limit should be < high limit"),
                      _CPTC("Pump::Low_limit")); 
    
    DEBUG_STREAM << " Pump::low_limit writing low_limit " 
        << this->name 
        << " LL1_EXT_FT18 address " 
        << this->low_limit_cmd
        << std::endl;

    SetReal (this->low_limit_cmd, val);

    this->low_limit = val;
}

  //- Get high limit ------------------
  float Pump::High_limit (void)
  {
    //DEBUG_STREAM << "Pump::High_limit Get value " << std::endl;

    if (!is_protocol_V2)
      THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"),
                      _CPTC("FT18 high limit only defined for V2 protocol!"),
                      _CPTC("Pump::High_limit")); 
 
    this->high_limit = GetReal(this->high_limit_offset);
    return this->high_limit;
  }

  //- Set High limit ------------------------
  void Pump::High_limit (float val)
  {
    if (!is_protocol_V2)
      THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"),
                      _CPTC("FT18 high limit only defined for V2 protocol!"),
                      _CPTC("Pump::High_limit")); 

    if (val < 0. || val > 20.)
      THROW_DEVFAILED(_CPTC("DATA_OUT_OF_RANGE"),
                      _CPTC("value out of range - should be in (l/min) [0.0 ... 20.0]"),
                      _CPTC("Pump::High_limit")); 

    //- High Limit should be > Low Limit
    if ( val <= this->low_limit)
      THROW_DEVFAILED(_CPTC("DATA_OUT_OF_RANGE"),
                      _CPTC("value out of range - high limit should be > low limit"),
                      _CPTC("Pump::High_limit")); 

    DEBUG_STREAM << " Pump::High_limit writing High_limit "
        << this->name 
        << " HL1_EXT_FT18 address " 
        << this->high_limit_cmd
        << std::endl;

    SetReal (this->high_limit_cmd, val);

    this->high_limit = val;
  }

  //- Get low limit tripped ------------------------
  bool Pump::LowLimitTripped (void)
  { 
    //DEBUG_STREAM << " Pump::LowLimitTripped entering " << std::endl;

    if (!is_protocol_V2)
      THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"),
                      _CPTC("FT18 low limit tripped only defined for V2 protocol!"),
                      _CPTC("Pump::LowLimitTripped")); 

   return GetBool (low_limit_tripped_bit_offset, low_limit_tripped_byte_offset); 
  }

  //- Get high limit ------------------------
  bool Pump::HighLimitTripped (void)
  { 
    //DEBUG_STREAM << " Pump::HighLimitTripped entering " << std::endl;

    if (!is_protocol_V2)
      THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"),
                      _CPTC("FT18 high limit tripped only defined for V2 protocol!"),
                      _CPTC("Pump::HighLimitTripped")); 

    return GetBool (high_limit_tripped_bit_offset, high_limit_tripped_byte_offset); 
  }

  //- State -------------------------------
  short Pump::State (void)
  {
    //DEBUG_STREAM << "Pump::State <-" << std::endl;

    if (get_com_state () != HWProxy_ns::HWP_NO_ERROR)
      return -1;

    this->state = GetInt (this->pump_status_offset);
    //- update the private members is_opened and is_closed

    if (this->state == 0)
    {
      this->is_on = false;
      this->is_off = true;
    }
    else if (state == 1) 
    {
      this->is_on = true;
      this->is_off = false;
    }
    else // add fault state management
    {
      this->is_on = false;
      this->is_off = false;
    }
    return this->state;
  }

  //- Status ------------------------------
  std::string Pump::Status (void)
  {
    //- communication error
    status = HWProxy_ns::hw_state_str [get_com_state ()];
    if (get_com_state () != HWProxy_ns::HWP_NO_ERROR)
      status += get_com_status ();
    else
      status += "\ndevice is up and running";

    short tmp = GetInt (this->pump_status_offset);
    if(tmp == 0)
      status += "\nPump OFF";
    else if (tmp == 1)
      status += "\nPump ON";
    else if (tmp == 2)
      status += "\nPump FAULT";
    else
      status += "\nUnknown Pump Status";
    return this->status;
  }

  //- On ----------------------------------
  void Pump::On ()
  {
    // Manage V2 protocol
    if (!is_protocol_V2)
    { // V1
      DEBUG_STREAM << " Pump::On " 
        << this->name 
        << " On address " 
        << this->on_byte_offset
        << "."
        << this->on_bit_offset
        << std::endl;

      SetBool (this->off_bit_offset, this->off_byte_offset, true);
      SetBool (this->on_bit_offset, this->on_byte_offset, true);
    }
    else
    { // V2
      DEBUG_STREAM << " Pump::On " 
        << this->name 
        << " On address " 
        << this->on_byte_offset
        << std::endl;

      SendCmd (on_byte_offset);
    }
  }

  //- Off ---------------------------------
  void Pump::Off ()
  {
    // Manage V2 protocol
    if (!is_protocol_V2)
    { // V1
      DEBUG_STREAM << " Pump::Off " 
        << this->name 
        << " Off address " 
        << this->off_byte_offset
        << "."
        << this->off_bit_offset
        << std::endl;

      SetBool (this->on_bit_offset, this->on_byte_offset, false);
      SetBool (this->off_bit_offset, this->off_byte_offset, false);
    }
    else
    { // V2
      DEBUG_STREAM << " Pump::Off " 
        << this->name 
        << " On address " 
        << this->off_byte_offset
        << std::endl;

      SendCmd (off_byte_offset);
    }
  }


  //---------------------------------------------------------------------
  //- HeaterVessel Class Implementation------------------------------------------
  //---------------------------------------------------------------------
  HeaterVessel::HeaterVessel (Tango::DeviceImpl * _host_device,
                               std::string _name) : 
                                Part (_host_device),
                                pressure_offset (PV_PT3_OFFSET),
                                setpoint_offset (C_CTRL_SP_IN_OFFSET),
                                pressure_auto_bit_offset (C_CTRL_AUT_BIT_OFFSET),
                                pressure_auto_byte_offset (C_CTRL_AUT_BYTE_OFFSET),
                                name (_name)
  {
    DEBUG_STREAM << "HeaterVessel::HeaterVessel building "
                 << this->name 
                 << std::endl;
    status = "initializing...";

    // Manage V2 protocol =====>
    pressure_ctrl_on = 0;
    pressure_ctrl_off = 0;
    pressure_sp = 0;

    if (is_protocol_V2)
    {
      pressure_sp = SET_PT3_SP;
      pressure_ctrl_on = HV_P3CTRL_ON;
      pressure_ctrl_off = HV_P3CTRL_OFF;
    }
    // <======
  }

  //- Dtor ------------------------------
  HeaterVessel::~HeaterVessel (void)
  {
    //- noop DTor
    DEBUG_STREAM << " HeaterVessel::~HeaterVessel deleting "
                 << this->name 
                 << std::endl;
  }

  //- Get Pressure ----------------------
  float HeaterVessel::Pressure (void)
  {
    //DEBUG_STREAM << "HeaterVessel::heater_vessel_pressure Get pressure " << std::endl;
    this->pressure = GetReal(this->pressure_offset);
    return this->pressure;
  }

  //- Set Pressure ----------------------
  void HeaterVessel::Setpoint (float val)
  {
    unsigned int db_addr = this->setpoint_offset;

    // Manage V2 protocol
    if (is_protocol_V2)
    {
      db_addr = this->pressure_sp;
    }

    DEBUG_STREAM << " HeaterVessel::heater_vessel_pressure Set pressure setpoint" 
      << this->name 
      << " C_CTRL_SP_IN address " 
      << db_addr
      << std::endl;

    if (val < 0. || val > 10.)
      THROW_DEVFAILED(_CPTC("DATA_OUT_OF_RANGE"),
                      _CPTC("value out of range - should be in (Bar) [0.0 ... 10.0]"),
                      _CPTC("HeaterVessel::Pressure")); 

    SetReal (db_addr, val);
  }

  //- Get setpoint ----------------------
  float HeaterVessel::Setpoint (void)
  {
    //DEBUG_STREAM << "HeaterVessel::heater_vessel_pressure Get pressure " << std::endl;

    this->setpoint = GetReal(this->setpoint_offset);
    return this->setpoint;
  }
 
  //- Get Pressure Auto State ------------
  bool HeaterVessel::PressureAuto (void)
  {
    //DEBUG_STREAM << " HeaterVessel::PressureAuto get PressureAuto state" << std::endl;

    this->pressure_auto = GetBool (this->pressure_auto_bit_offset, this->pressure_auto_byte_offset);
    return this->pressure_auto;
  }

  //- pressure_auto ---------------------
  void HeaterVessel::PressureAuto (bool val)
  {
    // Manage V2 protocol
    if (!is_protocol_V2)
    { // V1
      DEBUG_STREAM << " HeaterVessel::pressure_auto " 
        << this->name 
        << " On address " 
        << this->pressure_auto_byte_offset
        << "."
        << this->pressure_auto_bit_offset
        << std::endl;

      pressure_auto = val;
      SetBool (this->pressure_auto_bit_offset, this->pressure_auto_byte_offset, pressure_auto);
    }
    else
    { // V2
      unsigned int cmd = val ? this->pressure_ctrl_on : this->pressure_ctrl_off;
      DEBUG_STREAM << " HeaterVessel::pressure_auto " 
        << this->name 
        << " On address " 
        << cmd
        << std::endl;
      
      SendCmd(cmd);
    }
  }

  //- State -------------------------------
  short HeaterVessel::State (void)
  {
    //DEBUG_STREAM << "HeaterVessel::State <-" << std::endl;

    if (get_com_state () != HWProxy_ns::HWP_NO_ERROR)
      return -1;
    else 
      return 1;
  }

  //- Status ------------------------------
  std::string HeaterVessel::Status (void)
  {
    //- communication error
    //DEBUG_STREAM << "HeaterVessel::Status <-" << std::endl;

    status = HWProxy_ns::hw_state_str [get_com_state ()];
    if (get_com_state () != HWProxy_ns::HWP_NO_ERROR)
      status += get_com_status ();
    else
      status += "\ndevice is up and running";

    return this->status;
  }

} //- namespace
