
//- Project : Cryocooler, ACCEL/CRYOTHERM cryogenic cooler for optics at SOLEIL
//- file : DBOffsets.h
//- offsets for data in the PLC


#ifndef __DB_OFFSETS_H__
#define __DB_OFFSETS_H__
namespace CryoCooler_ns
{
  // ============================================================================
  // some defined enums and constants
  // ============================================================================

  //-----------------------------------------------------------------------------
  //- the offset values in the DB for V1 protocol
  //-----------------------------------------------------------------------------
  //- FlowMeter
  const unsigned int PV_FT18_OFFSET                = 0;   //- REAL
  //- Frequency converter 
  const unsigned int PV_LN2_FREQ_OFFSET            = 4;   //- REAL
  //- power
  const unsigned int POWER_OFFSET                  = 8;   //- REAL
  //- Level probes data
  const unsigned int PV_LT19_OFFSET                = 12;   //- REAL
  const unsigned int HL1_EXT_LT19_OFFSET           = 16;   //- REAL
  const unsigned int LL1_EXT_LT19_OFFSET           = 20;   //- REAL
  const unsigned int PV_LT23_OFFSET                = 24;   //- REAL
  const unsigned int HL1_EXT_LT23_OFFSET           = 28;   //- REAL
  const unsigned int LL1_EXT_LT23_OFFSET           = 32;   //- REAL
  //- Pressure probes data
  const unsigned int PV_PT1_OFFSET                 = 36;   //- REAL
  const unsigned int HL1_EXT_PT1_OFFSET            = 40;   //- REAL
  const unsigned int RESERVE_01_0FFSET             = 44;   //- REAL
  const unsigned int LL1_EXT_PT1_OFFSET            = 48;   //- REAL
  const unsigned int RESERVE_02_0FFSET             = 52;   //- REAL
  const unsigned int PV_PT3_OFFSET                 = 56;   //- REAL
  const unsigned int HL1_EXT_PT3_OFFSET            = 60;   //- REAL
  const unsigned int RESERVE_03_0FFSET             = 64;   //- REAL
  const unsigned int LL1_EXT_PT3_OFFSET            = 68;   //- REAL
  const unsigned int RESERVE_04_0FFSET             = 72;   //- REAL
  //- Temperature Data
  const unsigned int PV_T5_OFFSET                  = 76;   //- REAL
  const unsigned int HL1_EXT_T5_OFFSET             = 80;   //- REAL
  const unsigned int LL1_EXT_T5_OFFSET             = 84;   //- REAL
  const unsigned int PV_T6_OFFSET                  = 88;   //- REAL
  const unsigned int HL1_EXT_T6_OFFSET             = 92;   //- REAL
  const unsigned int LL1_EXT_T6_OFFSET             = 96;   //- REAL
  //- Analog Valves Data
  const unsigned int MV_MAN_V10_OFFSET             = 100;   //- REAL
  const unsigned int RESERVE_05_0FFSET             = 104;   //- REAL
  const unsigned int Q_MV_SCALE_V10_OFFSET         = 108;   //- REAL
  const unsigned int MV_MAN_V11_OFFSET             = 112;   //- REAL
  const unsigned int RESERVE_06_0FFSET             = 116;   //- REAL
  const unsigned int Q_MV_SCALE_V11_OFFSET         = 120;   //- REAL
  const unsigned int MV_MAN_V17_OFFSET             = 124;   //- REAL
  const unsigned int RESERVE_07_0FFSET             = 128;   //- REAL
  const unsigned int Q_MV_SCALE_V17_OFFSET         = 132;   //- REAL // !! RI doc inconsistency !!
  const unsigned int C_CTRL_SP_IN_OFFSET           = 136;   //- REAL
  const unsigned int C_CTRL_PV_OFFSET              = 140;   //- REAL
  const unsigned int CONV_MV_MAN_OFFSET            = 144;   //- REAL (LN2 setpoint) // !! RI doc inconsistency !!
  //- here goes some REAL reserve
  const unsigned int RESERVE_11_0FFSET             = 148;   //- REAL
  const unsigned int RESERVE_12_0FFSET             = 152;   //- REAL
  const unsigned int RESERVE_13_0FFSET             = 156;   //- REAL
  const unsigned int RESERVE_14_0FFSET             = 160;   //- REAL
  const unsigned int RESERVE_15_0FFSET             = 164;   //- REAL
  //- states of valves
  const unsigned int Q_OP_STATE_V9_OFFSET          = 168;   //- INT
  const unsigned int Q_OP_STATE_V15_OFFSET         = 170;   //- INT
  const unsigned int Q_OP_STATE_V19_OFFSET         = 172;   //- INT // !! RI doc inconsistency !!
  const unsigned int Q_OP_STATE_V20_OFFSET         = 174;   //- INT
  const unsigned int Q_OP_STATE_V21_OFFSET         = 176;   //- INT
  const unsigned int Q_OP_STATE_VENT_PIPE_OFFSET   = 178;   //- INT
  const unsigned int Q_OP_STATE_LN2_OFFSET         = 180;   //- INT
  const unsigned int Q_OP_STATE_V10_OFFSET         = 182;   //- INT
  const unsigned int Q_OP_STATE_V11_OFFSET         = 184;   //- INT
  const unsigned int Q_OP_STATE_V17_OFFSET         = 186;   //- INT // !! RI doc inconsistency !!
  //- here goes some reserve
  const unsigned int Q_RESERVE_24_OFFSET           = 186;   //- INT
  //- Digital Valves data
  const unsigned int VALV_MAN_ON_V9_BYTE_OFFSET    = 190;   //- BOOL
  const unsigned int VALV_MAN_ON_V9_BIT_OFFSET     = 0;     //- BOOL
  const unsigned int VALV_MAN_OFF_V9_BYTE_OFFSET   = 190;   //- BOOL
  const unsigned int VALV_MAN_OFF_V9_BIT_OFFSET    = 1;     //- BOOL
  const unsigned int VALV_MAN_ON_V15_BYTE_OFFSET   = 190;   //- BOOL
  const unsigned int VALV_MAN_ON_V15_BIT_OFFSET    = 2;     //- BOOL
  const unsigned int VALV_MAN_OFF_V15_BYTE_OFFSET  = 190;   //- BOOL
  const unsigned int VALV_MAN_OFF_V15_BIT_OFFSET   = 3;     //- BOOL
  //- set at each command
  const unsigned int ACTION_REQUEST_BYTE_OFFSET    = 190;   //- BOOL
  const unsigned int ACTION_REQUEST_BIT_OFFSET     = 4;     //- BOOL
  const unsigned int RESERVE_25_BYTE_OFFSET        = 190;   //- BOOL
  const unsigned int RESERVE_25_BIT_OFFSET         = 5;     //- BOOL
  //- Digital Valves data
  const unsigned int VALV_MAN_ON_V20_BYTE_OFFSET   = 190;   //- BOOL
  const unsigned int VALV_MAN_ON_V20_BIT_OFFSET    = 6;     //- BOOL
  const unsigned int VALV_MAN_OFF_V20_BYTE_OFFSET  = 190;   //- BOOL
  const unsigned int VALV_MAN_OFF_V20_BIT_OFFSET   = 7;     //- BOOL
  const unsigned int VALV_MAN_ON_V21_BYTE_OFFSET   = 191;   //- BOOL
  const unsigned int VALV_MAN_ON_V21_BIT_OFFSET    = 0;     //- BOOL
  const unsigned int VALV_MAN_OFF_V21_BYTE_OFFSET  = 191;   //- BOOL
  const unsigned int VALV_MAN_OFF_V21_BIT_OFFSET   = 1;     //- BOOL
  //- Frequency converter
  const unsigned int CONV_LC_OFF_BYTE_OFFSET       = 191;   //- BOOL
  const unsigned int CONV_LC_OFF_BIT_OFFSET        = 2;     //- BOOL
  const unsigned int CONV_LC_ON_BYTE_OFFSET        = 191;   //- BOOL
  const unsigned int CONV_LC_ON_BIT_OFFSET         = 3;     //- BOOL
  //- special valve : has the output visualisation
  const unsigned int VENT_PIPE_MAN_ON_BYTE_OFFSET  = 191;   //- BOOL
  const unsigned int VENT_PIPE_MAN_ON_BIT_OFFSET   = 4;     //- BOOL
  const unsigned int VENT_PIPE_MAN_OFF_BYTE_OFFSET = 191;   //- BOOL
  const unsigned int VENT_PIPE_MAN_OFF_BIT_OFFSET  = 5;     //- BOOL
  const unsigned int VENT_PIPE_ON_BYTE_OFFSET      = 191;   //- BOOL
  const unsigned int VENT_PIPE_ON_BIT_OFFSET       = 6;     //- BOOL
  //- Heater Vessel Pressure Automatic control
  const unsigned int C_CTRL_AUT_BYTE_OFFSET        = 191;   //- BOOL
  const unsigned int C_CTRL_AUT_BIT_OFFSET         = 7;     //- BOOL
  //- 
  const unsigned int STOP_ON_BYTE_OFFSET           = 192;   //- BOOL
  const unsigned int STOP_ON_BIT_OFFSET            = 0;     //- BOOL
  //- enables automatic refilling of SubCooler(V19)
  const unsigned int FILL_SUB_COOL_ON_BYTE_OFFSET  = 192;   //- BOOL
  const unsigned int FILL_SUB_COOL_ON_BIT_OFFSET   = 1;   //- BOOL
  //- starts the sequence to Stop the CryoCooler
  const unsigned int STOP_BUTTON_BYTE_OFFSET       = 192;   //- BOOL
  const unsigned int STOP_BUTTON_BIT_OFFSET        = 2;   //- BOOL
  const unsigned int CC_NO_ALARM_BYTE_OFFSET       = 192;   //- BOOL
  const unsigned int CC_NO_ALARM_BIT_OFFSET        = 3;   //- BOOL
  const unsigned int CC_ENABLE_BYTE_OFFSET         = 192;   //- BOOL
  const unsigned int CC_ENABLE_BIT_OFFSET          = 4;   //- BOOL
  const unsigned int CC_READY_BYTE_OFFSET          = 192;   //- BOOL
  const unsigned int CC_READY_BIT_OFFSET           = 5;   //- BOOL
  //- some reserve
  const unsigned int RESERVE_26_BYTE_OFFSET        = 192;   //- BOOL
  const unsigned int RESERVE_26_BIT_OFFSET         = 6;     //- BOOL
  const unsigned int RESERVE_27_BYTE_OFFSET        = 192;   //- BOOL
  const unsigned int RESERVE_27_BIT_OFFSET         = 7;     //- BOOL
  //- values tripped
  //- LT19
  const unsigned int LT19_H_LIM_VALUE_TRIPPED_BYTE_OFFSET = 193;
  const unsigned int LT19_H_LIM_VALUE_TRIPPED_BIT_OFFSET  = 0;
  const unsigned int LT19_L_LIM_VALUE_TRIPPED_BYTE_OFFSET = 193;
  const unsigned int LT19_L_LIM_VALUE_TRIPPED_BIT_OFFSET  = 1;
  const unsigned int LT19_H_ALM_VALUE_TRIPPED_BYTE_OFFSET = 193;
  const unsigned int LT19_H_ALM_VALUE_TRIPPED_BIT_OFFSET  = 2;
  const unsigned int LT19_L_ALM_VALUE_TRIPPED_BYTE_OFFSET = 193;
  const unsigned int LT19_L_ALM_VALUE_TRIPPED_BIT_OFFSET  = 3;
  //- LT23
  const unsigned int LT23_H_LIM_VALUE_TRIPPED_BYTE_OFFSET = 193;
  const unsigned int LT23_H_LIM_VALUE_TRIPPED_BIT_OFFSET  = 4;
  const unsigned int LT23_L_LIM_VALUE_TRIPPED_BYTE_OFFSET = 193;
  const unsigned int LT23_L_LIM_VALUE_TRIPPED_BIT_OFFSET  = 5;
  const unsigned int LT23_H_ALM_VALUE_TRIPPED_BYTE_OFFSET = 193;
  const unsigned int LT23_H_ALM_VALUE_TRIPPED_BIT_OFFSET  = 6;
  const unsigned int LT23_L_ALM_VALUE_TRIPPED_BYTE_OFFSET = 193;
  const unsigned int LT23_L_ALM_VALUE_TRIPPED_BIT_OFFSET  = 7;
  //- T5
  const unsigned int T5_H_LIM_VALUE_TRIPPED_BYTE_OFFSET = 194;
  const unsigned int T5_H_LIM_VALUE_TRIPPED_BIT_OFFSET  = 0;
  const unsigned int T5_L_LIM_VALUE_TRIPPED_BYTE_OFFSET = 194;
  const unsigned int T5_L_LIM_VALUE_TRIPPED_BIT_OFFSET  = 1;
  //- T6
  const unsigned int T6_H_LIM_VALUE_TRIPPED_BYTE_OFFSET = 194;
  const unsigned int T6_H_LIM_VALUE_TRIPPED_BIT_OFFSET  = 2;
  const unsigned int T6_L_LIM_VALUE_TRIPPED_BYTE_OFFSET = 194;
  const unsigned int T6_L_LIM_VALUE_TRIPPED_BIT_OFFSET  = 3;
  //- PT1
  const unsigned int PT1_H_LIM_VALUE_TRIPPED_BYTE_OFFSET = 194;
  const unsigned int PT1_H_LIM_VALUE_TRIPPED_BIT_OFFSET  = 4;
  const unsigned int PT1_L_LIM_VALUE_TRIPPED_BYTE_OFFSET = 194;
  const unsigned int PT1_L_LIM_VALUE_TRIPPED_BIT_OFFSET  = 5;
  //- PT3
  const unsigned int PT3_H_LIM_VALUE_TRIPPED_BYTE_OFFSET = 194;
  const unsigned int PT3_H_LIM_VALUE_TRIPPED_BIT_OFFSET  = 6;
  const unsigned int PT3_L_LIM_VALUE_TRIPPED_BYTE_OFFSET = 194;
  const unsigned int PT3_L_LIM_VALUE_TRIPPED_BIT_OFFSET  = 7;


  //-----------------------------------------------------------------------------
  //- the command values for V2 protocol
  //-----------------------------------------------------------------------------
  //- digital valve
  const unsigned int OPEN_V9        = 0x06;
  const unsigned int CLOSE_V9       = 0x07;
  const unsigned int OPEN_V15       = 0x08;
  const unsigned int CLOSE_V15      = 0x09;
  const unsigned int OPEN_V20       = 0x0C;
  const unsigned int CLOSE_V20      = 0x0D;
  const unsigned int OPEN_V21       = 0x0E;
  const unsigned int CLOSE_V21      = 0x0F;
  const unsigned int VP_HEAT_ON     = 0x13;
  const unsigned int VP_HEAT_OFF    = 0x14;
  const unsigned int OPEN_V17_35    = 0x1F; // in V2 protocol, the V17 valve is separated in 2 digital valves: V17.1 & V17.2
  const unsigned int CLOSE_V17_35   = 0x20;
  const unsigned int OPEN_V17_100   = 0x21;
  const unsigned int CLOSE_V17_100  = 0x22;
  //- analog valve
  const unsigned int SET_V10        = 0x10;
  const unsigned int SET_V11        = 0x11;
  const unsigned int SET_V19        = 0x12; // in V2 protocol, the V19 valve is managed as an adjustable valve (V19A)
  //- temperature
  const unsigned int SET_T5_HL1     = 0x54;
  const unsigned int SET_T5_LL1     = 0x55;
  const unsigned int SET_T6_HL1     = 0x56;
  const unsigned int SET_T6_LL1     = 0x57;
  //- pump
  const unsigned int SET_LN2_PUMP_FREQ  = 0x5F;
  const unsigned int LN2_PUMP_ON    = 0x17;
  const unsigned int LN2_PUMP_OFF   = 0x18;
  const unsigned int SET_FT18_HL1   = 0x58;
  const unsigned int SET_FT18_LL1   = 0x59;
  //- heater vessel
  const unsigned int HV_P3CTRL_ON   = 0x15;
  const unsigned int HV_P3CTRL_OFF  = 0x16;
  const unsigned int SET_PT3_SP     = 0x5E;
  //- pressure
  const unsigned int SET_PT1_HL1     = 0x5A;
  const unsigned int SET_PT1_LL1     = 0x5B;
  const unsigned int SET_PT3_HL1     = 0x5C;
  const unsigned int SET_PT3_LL1     = 0x5D;
  //- Level
  const unsigned int SET_LT19_HL1    = 0x50;
  const unsigned int SET_LT19_LL1    = 0x51;
  const unsigned int SET_LT23_HL1    = 0x52;
  const unsigned int SET_LT23_LL1    = 0x53;
  const unsigned int SET_LT19_MAN_MODE    = 0x19;
  const unsigned int SET_LT19_DECAY_MODE  = 0x1A;
  const unsigned int SET_LT19_TOPUP_MODE  = 0x23;

  //- General
  const unsigned int CC_OFF          = 0x05;

  //-----------------------------------------------------------------------------
  //- the specific offset values for V2 protocol
  //-----------------------------------------------------------------------------
  const unsigned int Q_OP_STATE_V19_OFFSET_V2         = 186;   //- INT
  const unsigned int Q_OP_STATE_V17_35_OFFSET_V2      = 172;   //- INT
  const unsigned int Q_OP_STATE_V17_100_OFFSET_V2     = 200;   //- INT
  const unsigned int Q_MV_SCALE_V19_OFFSET_V2         = 132;   //- REAL

  const unsigned int HL1_EXT_FT18_OFFSET           = 112;   //- REAL
  const unsigned int LL1_EXT_FT18_OFFSET           = 116;   //- REAL
  //- not specified in RI documentation but in RI read java tester:
  const unsigned int FT18_H_LIM_VALUE_TRIPPED_BYTE_OFFSET = 195; 
  const unsigned int FT18_H_LIM_VALUE_TRIPPED_BIT_OFFSET  = 0;
  const unsigned int FT18_L_LIM_VALUE_TRIPPED_BYTE_OFFSET = 195;
  const unsigned int FT18_L_LIM_VALUE_TRIPPED_BIT_OFFSET  = 1;

  const unsigned int LT19_MAN_MODE_ON_BYTE_OFFSET         = 192;
  const unsigned int LT19_MAN_MODE_ON_BIT_OFFSET          = 1;
  const unsigned int LT19_DECAY_MODE_ON_BYTE_OFFSET       = 192;
  const unsigned int LT19_DECAY_MODE_ON_BIT_OFFSET        = 2;
  const unsigned int LT19_TOPUP_MODE_ON_BYTE_OFFSET       = 192;
  const unsigned int LT19_TOPUP_MODE_ON_BIT_OFFSET        = 6;

  const unsigned int UPS_NO_POWER_FAIL_BYTE_OFFSET       = 192;
  const unsigned int UPS_NO_POWER_FAIL_BIT_OFFSET        = 7;
  const unsigned int SENSOR_CABLE_CONN_OK_BYTE_OFFSET    = 167;
  const unsigned int SENSOR_CABLE_CONN_OK_BIT_OFFSET     = 5;
  const unsigned int VALVE_CABLE_CONN_OK_BYTE_OFFSET     = 167;
  const unsigned int VALVE_CABLE_CONN_OK_BIT_OFFSET      = 6;
  //- not specified in RI documentation but in RI read java tester:  
  const unsigned int HEATER_CABLE_CONN_OK_BYTE_OFFSET    = 167;
  const unsigned int HEATER_CABLE_CONN_OK_BIT_OFFSET     = 7;
  
  //-----------------------------------------------------------------------------
  // V2 protocol interface for commands (byte offsets)
  //-----------------------------------------------------------------------------
  const unsigned int V2_COMMAND_REQUEST_BYTE = 0;
  const unsigned int V2_COMMAND_REQUEST_DBW_SIZE = 3;  // in words (3 x 2 bytes)

} //- namespace

#endif //- __DB_OFFSETS_H__
