
//- Project : Cryocooler, ACCEL/CRYOTHERM cryogenic cooler for optics at SOLEIL
//- file : Parts.h
//- classes handling the parts of the CryoCooler

#ifndef __PARTS_H__
#define __PARTS_H__

//-
#include <HWProxy.h>
#include "CryoCoolerInterface.h"
#include <yat/memory/SharedPtr.h>


namespace CryoCooler_ns
{

  //---------------------------------------------------------------------


  //---------------------------------------------------------------------  
  //- Parts.h
  //- Base class to get/set data from the HWProxy thread
  //- communicates with the class whitch handles the read polling og the PLC
  //- and the write service
  //---------------------------------------------------------------------
  class Part : public Tango::LogAdapter
  {
    public : 
      Part (Tango::DeviceImpl * _host_device);
      virtual ~Part (void);

      //- get a REAL (siemens PLC float 32 bits value)
      //- must get 2 consecutive WORDS (siemens unsigned 16 bits value)
      float GetReal (unsigned int offset);
      //- get a INT (siements PLC 16 bit signed value)
      short GetInt (unsigned int offset);
      //- get a bool 
      bool GetBool (unsigned int bit_offset, unsigned int byte_offset);

      //- set a REAL
      void SetReal (unsigned int offset, float v);
      //- set a bool
      void SetBool (unsigned int bit_offset, unsigned int byte_offset, bool val);

      // V2 protocol specific 
      //- send a command
      void SendCmd (unsigned int offset);

      //- the status string accessor
      std::string get_com_status ();
      //- the state of communication accessor
      HWProxy_ns::ComState get_com_state ();

    protected : 
      //- the data "getter" class
      yat::SharedPtr<CryoCoolerInterface> cryo_itf;
      //- status and state data;
      std::string com_status;
      HWProxy_ns::ComState com_state;
      // cryocooler protocol version
      bool is_protocol_V2;
     
    private :
      //- the host device 
      Tango::DeviceImpl * host_dev;
      //- the communication status string
  };


 //---------------------------------------------------------------------
  //- CryoCooler handles general information
  //- creates the thread
 //---------------------------------------------------------------------
  class CryoCoolerCom : public Tango::LogAdapter
  {
  public :
    //- CTor
    CryoCoolerCom (size_t _periodic_exec_ms, 
                Tango::DeviceImpl * _devimpl,
                std::string _plc_name, 
                short _dbread,
                short _dbwrite,
                std::string _cryoCoolerProtocol);

    //- DTor
    virtual ~CryoCoolerCom ();

    void init (void);
    unsigned long ComSuccess (void);
    unsigned long ComError (void);
    HWProxy_ns::ComState GetComState (void);
    std::string GetComStatus (void);

    bool StopOn (void);
    void StopButton (bool);
    bool NoAlarm (void);
    bool Enabled (void);
    bool Ready (void);
    bool UpsNoPowerFail (void);
    bool SensorCableConnected (void);
    bool ValveCableConnected (void);
    bool HeaterCableConnected (void);

    //- public statis instance
    static  yat::SharedPtr<CryoCoolerInterface> get_cryo_itf ();
	
  private :
    static yat::SharedPtr<CryoCoolerInterface> cryo_itf;
    size_t period;
    Tango::DeviceImpl * devimpl;
    std::string plc_name;
    short db_read_number;
    short db_write_number;
    std::string last_error;
    HWProxy_ns::ComState com_state;

    //- hardware readbacks
    bool stop_on;
    bool stop_button;
    bool no_alarm;
    bool enable;
    bool ready;

    //- offsets
    unsigned int stop_on_byte_offset;
    unsigned int stop_on_bit_offset;
    unsigned int stop_button_byte_offset;
    unsigned int stop_button_bit_offset;
    unsigned int no_alarm_byte_offset;
    unsigned int no_alarm_bit_offset;
    unsigned int enabled_byte_offset;
    unsigned int enabled_bit_offset;
    unsigned int ready_byte_offset;
    unsigned int ready_bit_offset;
    unsigned int ups_no_fail_byte_offset;
    unsigned int ups_no_fail_bit_offset;	
    unsigned int sensor_conn_byte_offset;
    unsigned int sensor_conn_bit_offset;	
    unsigned int valve_conn_byte_offset;
    unsigned int valve_conn_bit_offset;
    unsigned int heater_conn_byte_offset;
    unsigned int heater_conn_bit_offset;
	
   // cryocooler protocol version
   std::string cryocooler_protocol;
  };


  //----------------------------------------------------------------------------
  //- DigitalValve handles digital valves (V9, V15, V17.1, V17.2, V19, V20, V21)
  //----------------------------------------------------------------------------
  class DigitalValve : public Part
  {
    public : 
      DigitalValve (Tango::DeviceImpl * _host_device,
                    std::string _name);
      ~DigitalValve ();
      void Open (void);
      void Close (void);
      short State (void);
      std::string Status (void);
      std::string Name (void) { return this->name; }

    private : 
      //- the bit and byte offsets of the data in the DB (from the beginning)
      unsigned int man_on_bit_offset;
      unsigned int man_on_byte_offset;
      unsigned int man_off_bit_offset;
      unsigned int man_off_byte_offset;
      unsigned int status_byte_offset;	  
      //- the name of the valve 
      std::string name;

      //- the data to get/set
      bool is_opened;
      bool is_closed;
      short state;
      std::string status;
      bool setup_error;
      //- triggers a read of the data from the thread
  };

  //-------------------------------------------------------------------------
  //- AnalogValve handles analog (proportional) valves (V10, V11, V17, V19A)
  //-------------------------------------------------------------------------
  class AnalogValve : public Part
  {
    public : 
      AnalogValve (Tango::DeviceImpl * _host_device,
                   std::string name);
      ~AnalogValve ();
      //- open the valve to val
      void Open (float val);
      //- closes the valve
      void Close (void);
      short State (void);
      std::string Status (void);
      float Scaled (void);

    private : 

      //- the byte offsets of the data in the DB (from the beginning)
      unsigned int man_setpoint_offset;
      unsigned int status_offset;
      unsigned int scaled_offset;
      //- the name of the valve 
      std::string name;

      float setpoint;
      short state;
      float scaled;

      bool setup_error;
      std::string status;
  };

  //---------------------------------------------------------------------
  //- Temperature parts (T5, T6)
  //---------------------------------------------------------------------
  class Temperature : public Part
  {
    public : 
      Temperature (Tango::DeviceImpl * _host_device,
                          std::string name);
      ~Temperature ();

      //- get/set values
      float Value (void);
      float Low_limit (void);
      void Low_limit (float);
      float High_limit (void);
      void High_limit (float);
      bool LowLimitTripped (void);
      bool HighLimitTripped (void);

      short State (void);
      std::string Status (void);

    private : 
      //- the byte offsets of the data in the DB (from the beginning)
      unsigned int value_offset;
      unsigned int low_limit_offset;
      unsigned int high_limit_offset;
      unsigned int high_limit_tripped_byte_offset;
      unsigned int high_limit_tripped_bit_offset;
      unsigned int low_limit_tripped_byte_offset;
      unsigned int low_limit_tripped_bit_offset;
      // V2 protocol specific offsets
      unsigned int low_limit_cmd;
      unsigned int high_limit_cmd;
      //- the name of the temperature or level probe
      std::string name;
      bool setup_error;
      std::string status;
   
      float value;
      float low_limit;
      float high_limit;
  };


  //---------------------------------------------------------------------
 //- Level parts (LT19, LT23)
 //---------------------------------------------------------------------
  class Level : public Part
  {
    public : 
      Level (Tango::DeviceImpl * _host_device,
                          std::string name);
      ~Level ();

      //- get/set values
      float Value (void);
      float Low_limit (void);
      void Low_limit (float);
      float High_limit (void);
      void High_limit (float);
      bool LowLimitTripped (void);
      bool HighLimitTripped (void);
      bool LowAlarmTripped (void);
      bool HighAlarmTripped (void);
      short State (void);
      std::string Status (void);
      // V2 specific
      bool ManModeOn (void);
      bool DecayModeOn (void);
      bool TopupModeOn (void);
      void SetManMode (void);
      void SetDecayMode (void);
      void SetTopupMode (void);
 
    private : 
      //- the byte offsets of the data in the DB (from the beginning)
      unsigned int value_offset;
      unsigned int low_limit_offset;
      unsigned int high_limit_offset;
      unsigned int high_limit_tripped_byte_offset;
      unsigned int high_limit_tripped_bit_offset;
      unsigned int low_limit_tripped_byte_offset;
      unsigned int low_limit_tripped_bit_offset;

      unsigned int high_alarm_tripped_byte_offset;
      unsigned int high_alarm_tripped_bit_offset;
      unsigned int low_alarm_tripped_byte_offset;
      unsigned int low_alarm_tripped_bit_offset;

      // V2 protocol specific offsets
      unsigned int manual_mode_on_byte_offset;
      unsigned int manual_mode_on_bit_offset;
      unsigned int decay_mode_on_byte_offset;
      unsigned int decay_mode_on_bit_offset;
      unsigned int topup_mode_on_byte_offset;
      unsigned int topup_mode_on_bit_offset;

      unsigned int low_limit_cmd;
      unsigned int high_limit_cmd;
      unsigned int manual_mode_cmd;
      unsigned int decay_mode_cmd;
      unsigned int topup_mode_cmd;

      //- the name of the temperature or level probe
      std::string name;
      bool setup_error;
      std::string status;

      float value;
      float low_limit;
      float high_limit;
  };

  //---------------------------------------------------------------------
  //- Pressure parts (PT1, PT3)
  //---------------------------------------------------------------------
  class Pressure : public Part
  {
    public : 
      Pressure (Tango::DeviceImpl * _host_device,
                std::string name);
      ~Pressure ();

      //- get/set values
      float Value (void);
      float Low_limit (void);
      void Low_limit (float);
      float High_limit (void);
      void High_limit (float);
      bool LowLimitTripped (void);
      bool HighLimitTripped (void);
      short State (void);
      std::string Status (void);
 
    private : 
      //- the byte offsets of the data in the DB (from the beginning)
      unsigned int value_offset;
      unsigned int low_limit_offset;
      unsigned int high_limit_offset;
      unsigned int high_limit_tripped_byte_offset;
      unsigned int high_limit_tripped_bit_offset;
      unsigned int low_limit_tripped_byte_offset;
      unsigned int low_limit_tripped_bit_offset;
      // V2 protocol specific offsets
      unsigned int low_limit_cmd;
      unsigned int high_limit_cmd;
      //- the name of the temperature or level probe
      std::string name;
      bool setup_error;
      std::string status;

      float value;
      float low_limit;
      float high_limit;
  };

   //---------------------------------------------------------------------
  //- Pump part (LN2)
  //- handles Pump control, FT18, Power
  //---------------------------------------------------------------------
  class Pump : public Part
  {
    public : 
      Pump (Tango::DeviceImpl * _host_device,
            std::string name);
      ~Pump ();

      //- get/set values
      float Flow (void);
      void Frequency (float val);
      float Frequency (void);
      float Power (void);
      void On (void);
      void Off (void);
      short State (void);
      std::string Status (void);
      // V2 specific
      float Low_limit (void);
      void Low_limit (float);
      float High_limit (void);
      void High_limit (float);
      bool LowLimitTripped (void);
      bool HighLimitTripped (void);

  private : 
      //- the byte offsets of the data in the DB (from the beginning)
      unsigned int flow_offset;
      unsigned int frequency_offset;
      unsigned int frequency_setpoint_offset;
      unsigned int power_offset;
      unsigned int pump_status_offset;
      unsigned int on_bit_offset;
      unsigned int on_byte_offset;
      unsigned int off_bit_offset;
      unsigned int off_byte_offset;
      // V2 specific
      unsigned int low_limit_offset;
      unsigned int high_limit_offset;
      unsigned int high_limit_tripped_byte_offset;
      unsigned int high_limit_tripped_bit_offset;
      unsigned int low_limit_tripped_byte_offset;
      unsigned int low_limit_tripped_bit_offset;
      // V2 protocol specific offsets
      unsigned int low_limit_cmd;
      unsigned int high_limit_cmd;

      //- the name of the LN2 (should be LN2!)
      std::string name;
      std::string status;

      float flow;
      float frequency;
      float power;
      short state;
      bool is_on;
      bool is_off;
      // V2 specific
      float high_limit;
      float low_limit;
  };

  //---------------------------------------------------------------------
  //- General functions part
  class HeaterVessel : public Part
  {
    public : 
      HeaterVessel (Tango::DeviceImpl * _host_device,
                    std::string name);
      ~HeaterVessel ();

      //- get/set values
      float Pressure (void);
      float Setpoint (void);
      void Setpoint (float val);

      //- automatic mode
      bool PressureAuto (void);
      void PressureAuto (bool);

      short State (void);
      std::string Status (void);

  private : 
      //- the byte offsets of the data in the DB (from the beginning)
      unsigned int pressure_offset;
      unsigned int setpoint_offset;
      unsigned int pressure_auto_bit_offset;
      unsigned int pressure_auto_byte_offset;
      // V2 protocol special offsets
      unsigned int pressure_ctrl_on;
      unsigned int pressure_ctrl_off;
      unsigned int pressure_sp;

      //- the name of the Cryo 
      std::string name;

      float pressure;
      float setpoint;
      bool pressure_auto;
      std::string status;
  };


}//- namespace


#endif //-__PARTS_H__
