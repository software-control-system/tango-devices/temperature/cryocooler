//=============================================================================
//
// file :         CryoDigitalValveClass.h
//
// description :  Include for the CryoDigitalValveClass root class.
//                This class is represents the singleton class for
//                the CryoDigitalValve device class.
//                It contains all properties and methods which the 
//                CryoDigitalValve requires only once e.g. the commands.
//			
// project :      TANGO Device Server
//
// $Author: stephle $
//
// $Revision: 1.1 $
//
// $Log: not supported by cvs2svn $
//
// copyleft :     European Synchrotron Radiation Facility
//                BP 220, Grenoble 38043
//                FRANCE
//
//=============================================================================
//
//  		This file is generated by POGO
//	(Program Obviously used to Generate tango Object)
//
//         (c) - Software Engineering Group - ESRF
//=============================================================================

#ifndef _CRYODIGITALVALVECLASS_H
#define _CRYODIGITALVALVECLASS_H

#include <tango.h>
#include <CryoDigitalValve.h>


namespace CryoDigitalValve_ns
{//=====================================
//	Define classes for attributes
//=====================================
//=========================================
//	Define classes for commands
//=========================================
class CloseClass : public Tango::Command
{
public:
	CloseClass(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out,
				   const char        *in_desc,
				   const char        *out_desc,
				   Tango::DispLevel  level)
	:Command(name,in,out,in_desc,out_desc, level)	{};

	CloseClass(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out)
	:Command(name,in,out)	{};
	~CloseClass() {};
	
	virtual CORBA::Any *execute (Tango::DeviceImpl *dev, const CORBA::Any &any);
	virtual bool is_allowed (Tango::DeviceImpl *dev, const CORBA::Any &any)
	{return (static_cast<CryoDigitalValve *>(dev))->is_Close_allowed(any);}
};



class OpenClass : public Tango::Command
{
public:
	OpenClass(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out,
				   const char        *in_desc,
				   const char        *out_desc,
				   Tango::DispLevel  level)
	:Command(name,in,out,in_desc,out_desc, level)	{};

	OpenClass(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out)
	:Command(name,in,out)	{};
	~OpenClass() {};
	
	virtual CORBA::Any *execute (Tango::DeviceImpl *dev, const CORBA::Any &any);
	virtual bool is_allowed (Tango::DeviceImpl *dev, const CORBA::Any &any)
	{return (static_cast<CryoDigitalValve *>(dev))->is_Open_allowed(any);}
};



//
// The CryoDigitalValveClass singleton definition
//

class CryoDigitalValveClass : public Tango::DeviceClass
{
public:
//	properties member data

//	add your own data members here
//------------------------------------

public:
	Tango::DbData	cl_prop;
	Tango::DbData	cl_def_prop;
	Tango::DbData	dev_def_prop;

//	Method prototypes
	static CryoDigitalValveClass *init(const char *);
	static CryoDigitalValveClass *instance();
	~CryoDigitalValveClass();
	Tango::DbDatum	get_class_property(string &);
	Tango::DbDatum	get_default_device_property(string &);
	Tango::DbDatum	get_default_class_property(string &);
	
protected:
	CryoDigitalValveClass(string &);
	static CryoDigitalValveClass *_instance;
	void command_factory();
	void get_class_property();
	void write_class_property();
	void set_default_property();

private:
	void device_factory(const Tango::DevVarStringArray *);
};


}	//	namespace CryoDigitalValve_ns

#endif // _CRYODIGITALVALVECLASS_H
