static const char *RcsId = "$Header: /users/chaize/newsvn/cvsroot/Vacuum/CryoCooler/src/ClassFactory.cpp,v 1.1 2009-03-09 17:16:18 stephle Exp $";
//+=============================================================================
//
// file :        ClassFactory.cpp
//
// description : C++ source for the class_factory method of the DServer
//               device class. This method is responsible to create
//               all class singletin for a device server. It is called
//               at device server startup
//
// project :     TANGO Device Server
//
// $Author: stephle $
//
// $Revision: 1.1 $
//
// $Log: not supported by cvs2svn $
//
// copyleft :    European Synchrotron Radiation Facility
//               BP 220, Grenoble 38043
//               FRANCE
//
//-=============================================================================
//
//  		This file is generated by POGO
//	(Program Obviously used to Generate tango Object)
//
//         (c) - Software Engineering Group - ESRF
//=============================================================================


#include <tango.h>
#include <CryoCoolerClass.h>
#include <CryoDigitalValveClass.h>
#include <CryoAnalogValveClass.h>
#include <TemperatureClass.h>
#include <LevelClass.h>
#include <PressureClass.h>
#include <LN2Class.h>
#include <HeaterVesselClass.h>

/**
 *	Create <******>Class singleton and store it in DServer object.
 */

void Tango::DServer::class_factory()
{
  //- CryoCooler MUST be instanciated first
	add_class(CryoCooler_ns::CryoCoolerClass::init("CryoCooler"));
  //- then other classes
	add_class(CryoDigitalValve_ns::CryoDigitalValveClass::init("CryoDigitalValve"));
	add_class(CryoAnalogValve_ns::CryoAnalogValveClass::init("CryoAnalogValve"));
	add_class(Temperature_ns::TemperatureClass::init("Temperature"));
	add_class(Level_ns::LevelClass::init("Level"));
	add_class(Pressure_ns::PressureClass::init("Pressure"));
	add_class(LN2_ns::LN2Class::init("LN2"));
	add_class(HeaterVessel_ns::HeaterVesselClass::init("HeaterVessel"));
}
