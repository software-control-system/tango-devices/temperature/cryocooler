from conan import ConanFile

class CryoCoolerRecipe(ConanFile):
    name = "cryocooler"
    executable = "ds_CryoCooler"
    version = "2.1.3"
    package_type = "application"
    user = "soleil"
    python_requires = "base/[>=1.0]@soleil/stable"
    python_requires_extend = "base.Device"

    license = "GPL-3.0-or-later"    
    author = "Jean Coquet"
    url = "https://gitlab.synchrotron-soleil.fr/software-control-system/tango-devices/temperature/cryocooler.git"
    description = "CryoCooler device"
    topics = ("control-system", "tango", "device")

    settings = "os", "compiler", "build_type", "arch"

    exports_sources = "CMakeLists.txt", "src/*"
    
    def requirements(self):
        self.requires("yat4tango/[>=1.0]@soleil/stable")
        
        self.requires("plcserverproxy/[>=1.0]@soleil/stable")
        if self.settings.os == "Linux":
            self.requires("crashreporting2/[>=1.0]@soleil/stable")
